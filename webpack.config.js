const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const { VueLoaderPlugin } = require('vue-loader')
const WebpackNotifierPlugin = require('webpack-notifier');

module.exports = {
    devtool: 'source-map',
    mode: process.env.NODE_ENV || 'production',
    entry: {
        main: [
            './src/main.js',
            './sass/main.scss',
        ],
        'main-unit-data': './src/main-unit-data.js',
        'main-combat-data': './src/main-combat-data.js',
    },
    output: {
        filename: '[name].js',
        path: __dirname + '/public/build',
    },
    resolve: {
        extensions: ['.js', '.json', '.vue', '.css', '.scss'],
        alias: {
            '@': path.join(__dirname, 'src'),
        },
    },
    plugins: [
        new WebpackNotifierPlugin(),
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: ['**/*', '!\.gitignore'],
        }),
        new VueLoaderPlugin(),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css',
        }),
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: [
                    'source-map-loader',
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env'],
                            plugins: ['@babel/plugin-proposal-object-rest-spread'],
                        },
                    },

                ],
            },
            {
                test: /\.vue$/,
                use: [
                    'vue-loader',
                ]
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            implementation: require('sass'),
                            sourceMap: true,
                        },
                    },
                ],
            },
        ],
    },
};
