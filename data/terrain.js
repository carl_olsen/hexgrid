export const cost = mergeDefault({
    default: {
        grass: 1,
        light_woods: 2,

        forest: 3,
        river: 2,
        water: 2,
    },

    vehicle: {},
    infantry: {},
});

export const trailCost = mergeDefault({
    default: {
        road: 0.5,
        dirt_road: 0.65,
        train: 0.25,
    },
    infantry: {
        trail: 0.5,
    },
});

export const traversable = mergeDefault({
    default: {
        grass: true,
        light_woods: true,
        forest: true,
        sand: true,
        water: true,
        water_deep: false,
        mountains: false,
        river: true,
        road: true,
        dirt_road: true,
        trail: true,
    },

});

export const transparent = mergeDefault({
    default: {
        grass: true,
        light_woods: false,
        forest: false,
        sand: true,
        water: true,
        water_deep: true,
        mountains: false,
    },
});

const defense = mergeDefault({
    default: {
        light_woods: 1,
        forest: 3,
        water: -2,
    },
    infantry: {
        light_woods: 2,
        forest: 4,
    },
    vehicle: {
        sand: -1,
    },
});

export const modifiers = {
    defense,
};

function mergeDefault(data) {
    for (let key in data) {
        data[key] = Object.assign({}, data.default, data[key]);
    }

    return data;
}
