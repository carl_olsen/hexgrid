const INFANTRY = 'infantry';
const VEHICLE = 'vehicle';
const ARMOR = 'armor';
const ARTILLERY = 'artillery';
const MECHANIZED = 'mechanized';

const units = {
    vehicle_recon: {
        name: 'Recon Vehicle',
        shortName: 'Veh Rec',
        type: VEHICLE,

        move: 6,
        sightRange: 5,
        attackRange: 1,

        stats: {
            attack: 7,
            defense: 11,
        },
        natoSymbols: [
            'recon',
        ],
    },

    // armor
    afv: {
        name: 'Light Tank',
        shortName: 'Tank L',
        type: ARMOR,

        move: 4,
        sightRange: 3,
        attackRange: 1,

        stats: {
            attack: 9,
            defense: 9,

            attack_vs: {
                [ARMOR]: 12,
            },
        },
        natoSymbols: [
            'armor-wheeled',
        ],
    },
    tank_medium: {
        name: 'Medium Tank',
        shortName: 'Tank M',
        type: ARMOR,

        move: 3,
        sightRange: 3,
        attackRange: 1,

        stats: {
            attack: 10,
            defense: 10,
            attack_vs: {
                [ARMOR]: 13,
            },
        },
        natoSymbols: [
            'armor',
        ],
    },
    tank_heavy: {
        name: 'Heavy Tank',
        shortName: 'Tank H',
        type: ARMOR,

        move: 2,
        sightRange: 3,
        attackRange: 1,

        stats: {
            attack: 11,
            defense: 11,

            attack_vs: {
                [ARMOR]: 14,
            },
        },

        natoSymbols: [
            'armor',
        ],

    },
    // infantry
    infantry: {
        name: 'Infantry',
        shortName: 'Inf',
        type: INFANTRY,

        move: 3,
        sightRange: 3,
        attackRange: 1,

        stats: {
            attack: 10,
            defense: 7,

            attack_vs: {
                [ARMOR]: 7,
            },
        },

        natoSymbols: [
            'infantry',
        ],
    },
    infantry_recon: {
        name: 'Recon Infantry',
        shortName: 'Inf Rec',
        type: INFANTRY,

        move: 3,
        sightRange: 5,
        attackRange: 1,

        stats: {
            attack: 7,
            defense: 7,

            attack_vs: {
                [INFANTRY]: 10,
            },
        },

        natoSymbols: [
            'recon',
        ],
    },
    infantry_assault: {
        name: 'Assault Infantry',
        shortName: 'Inf Assault',
        type: INFANTRY,

        move: 4,
        sightRange: 3,
        attackRange: 1,

        stats: {
            attack: 10,
            defense: 7,

            attack_vs: {
                [INFANTRY]: 11,
                [MECHANIZED]: 11,
            },

            defense_vs: {
                [INFANTRY]: 8,
            },
        },
    },
    infantry_mechanized: {
        name: 'Mechanized Infantry',
        shortName: 'Inf Mech',
        type: MECHANIZED,

        move: 6,
        sightRange: 3,
        attackRange: 1,

        stats: {
            attack: 11,
            defense: 8,

            defense_vs: {
                [ARMOR]: 7,
            },
        },
        natoSymbols: [
            'infantry',
            'armor',
        ],
    },
    infantry_anti_tank: {
        name: 'AT Infantry',
        shortName: 'Inf AT',
        type: INFANTRY,

        move: 3,
        sightRange: 3,
        attackRange: 1,

        stats: {
            attack: 7,
            defense: 8,

            attack_vs: {
                [ARMOR]: 16,
                [MECHANIZED]: 11,
            },
        },

        natoSymbols: [
            'anti-tank',
        ],
    },

    // artillery
    artillery_medium: {
        name: 'Medium Artillery',
        shortName: 'Art M',
        type: ARTILLERY,

        move: 1,
        sightRange: 1,
        attackMinRange: 4,
        attackRange: 10,

        stats: {
            attack: 9,
            defense: 5,
        },
        natoSymbols: [
            'artillery',
        ],
    },
};

const defaults = {
    hpMax: 10,
    stats: {},
    actions: ['attack'],
};

const defaultStats = {
    attack: 0,
    defense: 0,
    attack_vs: {},
    defense_vs: {},
};

for (let key in units) {
    let unit = units[key];
    unit = Object.assign({}, defaults, unit);
    unit.stats = Object.assign({}, defaultStats, unit.stats);
    unit.key = key;
    unit.natoSymbols = [].concat(unit.natoSymbols);
    unit.actions = [].concat(unit.actions);

    units[key] = unit;
}

module.exports = units;
