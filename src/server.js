let express = require('express');
let app = express();
let http = require('http').createServer(app);

let port = process.env.SERVER_PORT || 8000;

http.listen(port, () => {
    console.log('listening on *:'+port);
});

app.use(express.static('public'));
