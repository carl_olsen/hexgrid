
module.exports = {
    percentDistribution,
    rangeInt,
    range,

    randomPercentDistribution: percentDistribution,
    randomRange:           range,
    randomRangeInt:        rangeInt,

}




let randomDistribution = {
    '-5': 0.01,
    '-4': 0.03,
    '-3': 0.05,
    '-2': 0.10,
    '-1': 0.19,
    0:    0.24,
    1:    0.19,
    2:    0.10,
    3:    0.05,
    4:    0.03,
    5:    0.01,
};

function weightedList(list, rng, weightKey, valueKey) {
            rng = rng !== void 0 ? rng : this.rng;
            weightKey = weightKey || 'weight';
            valueKey = valueKey || 'value';

            let total = 0,
                i, item;

            for (i = 0; i < list.length; i++) {
                item = list[i];
                total += item[weightKey] || 0;
            }
            let random = Math.floor(rng() * total + 1),
                currentTotal = 0;

            for (i = 0; i < list.length; i++) {
                item = list[i];
                currentTotal += item[weightKey];
                if (random < currentTotal) {
                    return item[valueKey];
                }
            }
        }
function percentDistribution(list, rng = Math.random) {
    let sum = 0;
    let r   = rng();
    let result = list.find(({chance}) => {
    	sum += chance;
    	return r <= sum;
    });

    return result.value;

}

function range(min, max, rng = Math.random) {
    return rng() * (max - min) + min;
}

function rangeInt(min, max, rng = Math.random) {
    return Math.round(range(min, max, rng));
}
