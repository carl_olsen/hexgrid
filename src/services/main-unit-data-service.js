import Grid, {GRID_LAYOUTS} from '../hexgrid/grid';
import Unit from '@/game/unit'
import Combat from '@/rules/combat'

import units from '../../data/units';

let grassTile = {
    type: 'grass',
    subTypes: [],
};
let grid = Grid({
    hexSize: 1,
    layout: GRID_LAYOUTS.OddQ,
    width: 2,
    height: 2,
    tileData: {
        '0,0': grassTile,
        '0,1': grassTile,
        '1,0': grassTile,
        '1,1': grassTile,
    },
});

export function getData(sourceHp = 10, targetHp = 10) {
    let combatData = [];
    let out = [];
    let columns = [];

    let unitKeys = Object.keys(units);

    unitKeys.forEach(function(unitAKey) {

        let unitA = Unit({
            ownerId: 'player1',
            key: unitAKey,
        });
        unitA.hp = sourceHp;
        columns.push(unitA.name);

        unitA.tile = grid.get('0,0');
        let row = {
            source: unitAKey,
            sourceId: unitA.id,
            targets: {},
        };

        unitKeys.forEach(function(unitBKey) {

            let unitB = Unit({
                ownerId: 'player2',
                key: unitBKey,
            });
            unitB.tile = grid.get('0,1');
            unitB.hp = targetHp;

            let com = Combat({
                grid,
                source: unitA,
                target: unitB,
            });

            let result = com.getData();

            combatData.push(result);

            row.targets[unitBKey] = {
                sourceCombatValue: result.sourceCombatValue,
                targetCombatValue: result.targetCombatValue,
                combatId: combatData.length - 1,
            };

        });

        out.push(row);
    });

    return {
        combatById: combatData,
        results: out,
    };

}
