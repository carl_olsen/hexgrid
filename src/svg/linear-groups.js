import {coordsToSvgPathData} from '@/svg/util';
import sortBy from 'lodash/sortBy';

export default function linearGroupsToPaths(grid, linearGroups) {

    let subTypeOrder = {
        river: 0,
        trail: 1,
        dirt_road: 2,
        road: 3,
    };

    let subTypes = [];

    linearGroups.forEach(({subType, groups}) => {

        let lines = [];

        groups.forEach((group) => {
            lines = lines.concat(parseLines(grid, subType, group));
        });

        subTypes.push({
            subType,
            lines,
        });
    });

    subTypes = sortBy(subTypes, ({subType}) => {
        return subTypeOrder[subType];
    });

    let output = [];
    subTypes.forEach(({subType, lines}) => {
        lines.forEach((line) => {

            let siblingLineTiles = lines.filter((l) => l !== line);
            siblingLineTiles = siblingLineTiles.reduce((a, b) => a.concat(b), []);
            output.push({
                subType,
                path: getPath(grid, line, siblingLineTiles),
            });
        });
    });

    return output;
}

function parseLines(grid, subType, tiles) {

    let remainingTiles = new Map();
    let first = tiles.shift();
    let lines = [];

    tiles.forEach((tile) => remainingTiles.set(tile, true));
    traverse(first);

    function traverse(tile, line = []) {
        line.push(tile);

        remainingTiles.delete(tile);

        let neighbors = grid.neighbors(tile).filter((nTile) => {
            return remainingTiles.has(nTile);
        });

        if (!neighbors.length) {
            lines.push(line);
            return;
        }

        let first = neighbors.shift();
        traverse(first, line);

        neighbors.forEach((nTile) => {
            traverse(nTile, [tile]);

        });
    }

    return lines;
}

function getPath(grid, tiles, siblingLineTiles) {
    let coords = tiles.map((tile) => tile.pxCenter);
    let firstTile = tiles[0];
    let lastTile = tiles[tiles.length - 1];
    let first = getReverseSideCoord(grid, firstTile, tiles, siblingLineTiles);
    let last = getReverseSideCoord(grid, lastTile, tiles, siblingLineTiles);
    let before = [];
    let after = [];

    if (first) {
        before = [first];
    }
    if (last) {
        after = [last];
    }

    let path = [].concat(before, coords, after);
    return coordsToSvgPathData(path);
}

function getReverseSideCoord(grid, tile, tiles, siblingLineTiles) {
    // ignore intersections with other groups
    if (siblingLineTiles.indexOf(tile) !== -1) {
        return;
    }

    let nTile = grid.neighbors(tile).find(function(nTile) {
        return tiles.indexOf(nTile) !== -1;
    });

    if (!nTile) {
        return;
    }

    let dir = grid.directionOfNeighbor(tile, nTile);
    let revDir = grid.reverseDirection(dir);
    let sideCenter = tile.pxSideCenters[revDir];

    return sideCenter;

}
