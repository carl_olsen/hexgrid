import * as d3 from 'd3';
import {outliner} from '@/hexgrid/outliner';

export function coordsToSvgPathData(coords, path = null) {
    coords = [].concat(coords);
    path = path || d3.path();

    let {x, y} = coords.shift();

    path.moveTo(x, y);

    coords.forEach(function({x, y}) {
        path.lineTo(x, y);
    });

    return path;
}

export function getReverseTileOutline(grid, tiles, boundsWidth, boundsHeight) {
    let tile = tiles[0];
    let tw = tile.hexagonWidth;
    let th = tile.hexagonHeight;

    let polyGroups = outliner(grid, tiles);
    let path = d3.path();

    let x = 0;
    let y = 0;
    let w = boundsWidth;
    let h = boundsHeight;

    // zero
    x -= tw * 0.5;
    y -= th * 0.5;
    w -= tw * 0.5;
    h -= th * 0.5;

    // expand
    x -= tw;
    y -= th;
    w += tw;
    h += th;

    path.moveTo(x, y);
    path.lineTo(w, y);
    path.lineTo(w, h);
    path.lineTo(x, h);
    path.closePath();

    polyGroups.forEach((polyGroup) => {
        polyGroup.forEach((paths) => {
            coordsToSvgPathData(paths, path);
            path.closePath();
        });
    });

    return path;
}

export function tilesToOutline(grid, tiles) {
    if (!tiles.length) {
        return [];
    }
    let polyGroups = outliner(grid, tiles);

    return polyGroups.map((polyGroup) => {
        let pathStr = '';
        polyGroup.forEach((paths) => {
            pathStr += coordsToSvgPathData(paths);
        });
        return pathStr;
    });
}

