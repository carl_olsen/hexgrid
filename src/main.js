import Vue from 'vue';
import Game from '@/vue/components/Game';
import makeStore from '@/vue/store';
import mapData from '../data/maps/demo.json';
import {loadMap} from '@/game/map/map-parser';
import makeGameStateManager from '@/states/state-manager';
import {SELECT_UNIT_STATE} from '@/states/select-unit-state';

let {grid, unitsData} = loadMap(mapData);

let store = makeStore({grid});
store.commit('units/setAll', unitsData);
store.dispatch('turn/newTurn');

let stateManager = makeGameStateManager({store, grid});
stateManager.set(SELECT_UNIT_STATE);

new Vue({
    store,
    el: '#game-container',
    render: h => h(Game),
    provide() {
        return {
            grid,
        };
    },
});
