export default function Cube(x, y, z) {
    this.x = x;
    this.y = y;
    this.z = z;
    this.cubeId = [x, y, z].join(',')
};

Cube.prototype = {
    cubeId: null,
    x: null,
    y: null,
    z: null,
};

export function cubeToId({x, y, z}) {
    return [x, y, z].join(',');
}

Cube.add = function(a, b) {
    let x, y, z;
    if (typeof b === 'number') {
        x = a.x + b;
        y = a.y + b;
        z = a.z + b;
    } else {
        x = a.x + b.x;
        y = a.y + b.y;
        z = a.z + b.z;
    }
    return new Cube(x, y, z);
};

Cube.multiply = function(a, b) {
    let x, y, z;
    if (typeof b === 'number') {
        x = a.x * b;
        y = a.y * b;
        z = a.z * b;
    } else {
        x = a.x * b.x;
        y = a.y * b.y;
        z = a.z * b.z;
    }

    return new Cube(x, y, z);
};

Cube.distance = function(a, b) {
    return Math.floor((Math.abs(a.x - b.x) + Math.abs(a.y - b.y) + Math.abs(a.z - b.z)) / 2);
};

Cube.direction = function(direction) {
    return Cube.directions[direction];
};

Cube.reverseDirection = function(direction) {
    let i = direction + 3;
    if (i > 5) {
        i = i - 6;
    }
    return i;
};

Cube.neighbor = function(hex, direction) {
    return Cube.add(hex, Cube.directions[direction]);
};

Cube.neighbors = function(cube) {
    return Cube.directions
        .map(function(nCube) {
            return Cube.add(cube, nCube);
        });
};

Cube.diagonalNeighbor = function(hex, direction) {
    return Cube.add(hex, Cube.diagonals[direction]);
};

Cube.cubeRound = function(h) {
    let rx = Math.round(h.x);
    let ry = Math.round(h.y);
    let rz = Math.round(h.z);
    let x_diff = Math.abs(rx - h.x);
    let y_diff = Math.abs(ry - h.y);
    let z_diff = Math.abs(rz - h.z);
    if (x_diff > y_diff && x_diff > z_diff)
        rx = -ry - rz;
    else if (y_diff > z_diff)
        ry = -rx - rz; else
        rz = -rx - ry;
    return new Cube(rx, ry, rz);
};

Cube.cubeLerp = function(a, b, t) {
    return new Cube(a.x + (b.x - a.x) * t, a.y + (b.y - a.y) * t, a.z + (b.z - a.z) * t);
};

Cube.cubeLine = function(a, b) {
    let N = Cube.distance(a, b);
    let results = [];
    let i = 0;
    while (i < N) {
        i++;
        results.push(Cube.cubeRound(Cube.cubeLerp(a, b, 1.0 / Math.max(1, N) * i)));
    }
    return results;
};

Cube.inRadius = function(cube, radius) {
    let N = radius;

    let results = [];

    for (let dx = -N; dx <= N; dx++) {

        let min = Math.max(-N, -dx - N);
        let max = Math.min(N, -dx + N);

        for (let dy = min; dy <= max; dy++) {
            let dz = -dx - dy;

            let delta = new Cube(dx, dy, dz);
            let newCube = Cube.add(cube, delta);

            results.push(newCube);
        }
    }

    return results;
};

Cube.ring = function(cube, distance) {
    let results = [];
    if (distance === 0) {
        return [cube];
    }

    let dir = Cube.directions[4];
    let vec = Cube.multiply(dir, distance);
    let cur = Cube.add(vec, cube);
    for (let i = 0; i < 6; i++) {
        for (let j = 0; j < distance; j++) {
            results.push(cur);
            cur = Cube.neighbor(cur, i);

        }
    }

    return results;
};

Cube.spiral = function(cube, distance) {
    let results = [cube];
    for (let i = 1; i <= distance; i++) {
        results = results.concat(Cube.ring(cube, i));
    }
    return results;
};

Cube.findNearest = function(cube, distance, findCallback) {
    for (let i = 1; i <= distance; i++) {
        let ring = Cube.ring(cube, i);
        let result = ring.find(findCallback);

        if (result !== undefined) {
            return result;
        }
    }
};

Cube.directions = Object.freeze([
    // SE
    new Cube(1, -1, 0),

    // S
    new Cube(0, -1, 1),

    // SW
    new Cube(-1, 0, 1),

    // NW
    new Cube(-1, 1, 0),

    // N
    new Cube(0, 1, -1),

    // NE
    new Cube(1, 0, -1),
]);

Cube.diagonals = Object.freeze([
    new Cube(2, -1, -1),
    new Cube(1, 1, -2),
    new Cube(-1, 2, -1),
    new Cube(-2, 1, 1),
    new Cube(-1, -1, 2),
    new Cube(1, -2, 1),
]);
