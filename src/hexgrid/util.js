// The shape of a hexagon is adjusted by the scale; the rotation is handled elsewhere, using svg transforms
export function hexagonPathString(scale, orientation) {
    return hexagonPathCoords(scale, orientation)
        .map(function(p) {
            return p.x.toFixed(3) + ',' + p.y.toFixed(3);
        }).join(' ');
}

// scale should be the distance from corner to corner
// orientation should be 0 (flat bottom hex) or 1 (flat side hex)
// corner coord order
// east
// south east
// south west
// west
// north west
// north east
export function hexagonPathCoords(scale, orientation) {

    let points = [];

    for (let i = 0; i < 6; i++) {

        let angleDeg = 60 * i;

        if (orientation) {
            angleDeg += 30;
        }

        let angleRad = Math.PI / 180 * angleDeg;

        points.push({
            x: 0.5 * scale * Math.cos(angleRad),
            y: 0.5 * scale * Math.sin(angleRad),
        });
    }
    return points;

}

export function hexagonCornersToSides(pxCorners) {
    let pxSides = [];

    for (let i = 0; i < 6; i++) {
        let aId = i;
        let bId = i + 1;

        if (i === 5) {
            bId = 0;
        }

        let cornerA = pxCorners[aId];
        let cornerB = pxCorners[bId];
        let side = [cornerA, cornerB];

        pxSides.push(side);
    }
    return pxSides;
}

export function boundsOfPoints(points) {
    let minX = 0.0;
    let minY = 0.0;
    let maxX = 0.0;
    let maxY = 0.0;
    let i = 0;
    while (i < points.length) {
        let p = points[i];
        ++i;
        if (p.x < minX)
            minX = p.x;
        if (p.x > maxX)
            maxX = p.x;
        if (p.y < minY)
            minY = p.y;
        if (p.y > maxY)
            maxY = p.y;
    }
    return {
        minX: minX,
        maxX: maxX,
        minY: minY,
        maxY: maxY,
    };
}

export function lineCenter(p1, p2) {
    return {
        x: (p1.x + p2.x) * 0.5,
        y: (p1.y + p2.y) * 0.5,
    };
}

// angle between center of t1 and t2 vertices
export function tileCenterToCornersRange(t1, t2) {
    let t1Center = t1.pxCenter;
    let t2Corners = t2.pxCorners;
    let min;
    let max;

    let rads = t2Corners.map(function(vertice) {
        return coordsToRad(t1Center, vertice);
    });

    min = Math.min.apply(null, rads);
    max = Math.max.apply(null, rads);

    if (max - min > Math.PI) {
        rads = rads.map(function(angle) {
            if (angle > Math.PI) {
                angle -= 2 * Math.PI;
            }
            return angle;
        });
        min = Math.min.apply(null, rads);
        max = Math.max.apply(null, rads);
    }

    return [min, max];
}

export function degToRad(deg) {
    return deg % 360 * Math.PI / 180;
}

export function radToDeg(rad) {
    return Math.round((rad * 180 / Math.PI % 360) * 1000) / 1000;
}

export function degToDirection(deg) {
    return Math.round((deg - 30) / 60) % 6;
}

export function radToDirection(rad) {
    let deg = radToDeg(rad);
    return degToDirection(deg);
}

export function directionToDeg(dir) {
    return Math.round((dir * 60) + 30);
}

export function directionToRad(dir) {
    return degToRad(directionToDeg(dir));
}

export function coordsToRad(a, b) {
    let dx = b.x - a.x;
    let dy = b.y - a.y;

    let rad;
    if (dx === 0) {
        rad = Math.PI / 2.0 * (dy > 0 ? 1 : -1);
    } else {
        rad = Math.atan(dy / dx);
        if (dx < 0) {
            rad += Math.PI;
        }
    }

    if (rad < 0) {
        rad += 2.0 * Math.PI;
    }
    return rad;
}
