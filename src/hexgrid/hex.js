export default function Hex(q, r) {
    this.q = q;
    this.r = r;
    this.id = this.q + ',' + this.r;
}

Hex.prototype = {
    id: null,
    q: null,
    r: null,
};

export function hexToId({q, r}) {
    return [q, r].join(',');
}
