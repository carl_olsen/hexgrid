import {cubeToId} from '@/hexgrid/cube';

export default function({id, q, r, x, y, z, pxCenter, pxCorners, pxSides, pxSideCenters, hexagonHeight, hexagonWidth, type, subTypes}) {

    let cubeId = cubeToId({x, y, z});
    return Object.freeze({
        q,
        r,
        x,
        y,
        z,
        pxCenter,
        pxCorners,
        pxSides,
        pxSideCenters,
        hexagonHeight,
        hexagonWidth,
        id,
        cubeId,
        type,
        subTypes,
    });
}
