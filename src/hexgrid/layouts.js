import Cube from '@/hexgrid/cube';
import Hex from '@/hexgrid/hex';

let ORIENTATION = {
    FLAT_TOP: 0,
    FLAT_SIDE: 1,
};
// directions map to Cube.directions
let Q = {
    directionIds: {
        SE: 0,
        S: 1,
        SW: 2,
        NW: 3,
        N: 4,
        NE: 5,
    },

    directionNames: [
        'SE',
        'S',
        'SW',
        'NW',
        'N',
        'NE',
    ],

    cornerIds: {
        E: 0,
        SE: 1,
        SW: 2,
        W: 3,
        NW: 4,
        NE: 5,
    },

    cornerNames: [
        'E',
        'SE',
        'SW',
        'W',
        'NW',
        'NE',
    ],
};

let R = {
    directionIds: {
        E: 0,
        SE: 1,
        SW: 2,
        W: 3,
        NW: 4,
        NE: 5,
    },

    directionNames: [
        'E',
        'SE',
        'SW',
        'W',
        'NW',
        'NE',
    ],

    cornerIds: {
        SE: 0,
        S: 1,
        SW: 2,
        NW: 3,
        N: 4,
        NE: 5,
    },

    cornerNames: [
        'SE',
        'S',
        'SW',
        'NW',
        'W',
        'NE',
    ],
};

export default {
    ORIENTATION: ORIENTATION,
    EvenR: {
        name: 'EvenR',
        hexToCube: function(hex) {
            let z = hex.r;
            let x = hex.q - (hex.r + (hex.r & 1) >> 1);
            return new Cube(x, -x - z, z);
        },
        cubeToHex: function(cube) {
            let x = cube.x | 0;
            let z = cube.z | 0;
            return new Hex(x + (z + (z & 1) >> 1), z);
        },
        cubeToHexId: function(cube) {
            let x = cube.x | 0;
            let z = cube.z | 0;
            x = x + (z + (z & 1) >> 1);
            let y = z;
            return x + ',' + y;
        },
        orientation: ORIENTATION.FLAT_SIDE,
        directionIds: R.directionIds,
        directionNames: R.directionNames,
        cornerIds: R.cornerIds,
        cornerNames: R.cornerNames,
    },
    OddR: {
        name: 'OddR',
        hexToCube: function(hex) {
            let z = hex.r;
            let x = hex.q - (hex.r - (hex.r & 1) >> 1);
            return new Cube(x, -x - z, z);
        },
        cubeToHex: function(cube) {
            let x = cube.x | 0;
            let z = cube.z | 0;
            return new Hex(x + (z - (z & 1) >> 1), z);
        },
        cubeToHexId: function(cube) {
            let x = cube.x | 0;
            let z = cube.z | 0;
            x = x + (z - (z & 1) >> 1);
            let y = z;
            return x + ',' + y;
        },
        orientation: ORIENTATION.FLAT_SIDE,
        directionIds: R.directionIds,
        directionNames: R.directionNames,
        cornerIds: R.cornerIds,
        cornerNames: R.cornerNames,
    },
    EvenQ: {
        name: 'EvenQ',
        hexToCube: function(hex) {
            let x = hex.q;
            let z = hex.r - (hex.q + (hex.q & 1) >> 1);
            return new Cube(x, -x - z, z);
        },
        cubeToHex: function(cube) {
            let x = cube.x | 0;
            let z = cube.z | 0;
            return new Hex(x, );
        },
        cubeToHexId: function(cube) {
            let x = cube.x | 0;
            let z = cube.z | 0;
            let y = z + (x + (x & 1) >> 1);
            return x + ',' + y;
        },
        orientation: ORIENTATION.FLAT_TOP,
        directionIds: Q.directionIds,
        directionNames: Q.directionNames,
        cornerIds: Q.cornerIds,
        cornerNames: Q.cornerNames,
    },
    OddQ: {
        name: 'OddQ',
        hexToCube: function(hex) {
            let x = hex.q;
            let z = hex.r - (hex.q - (hex.q & 1) >> 1);
            return new Cube(x, -x - z, z);
        },
        cubeToHex: function(cube) {
            let x = cube.x | 0;
            let z = cube.z | 0;
            return new Hex(x, z + (x - (x & 1) >> 1));
        },
        cubeToHexId: function(cube) {
            let x = cube.x | 0;
            let z = cube.z | 0;
            let y = z + (x - (x & 1) >> 1);
            return x + ',' + y;
        },
        orientation: ORIENTATION.FLAT_TOP,
        directionIds: Q.directionIds,
        directionNames: Q.directionNames,
        cornerIds: Q.cornerIds,
        cornerNames: Q.cornerNames,
    },
};
