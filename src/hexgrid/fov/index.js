import Arc from '@/hexgrid/fov/arc';
import AngleOfView from '@/hexgrid/fov/angle-of-view';
import {tileCenterToCornersRange} from '@/hexgrid/util';
import {terrainTransparent} from '@/rules/terrain';

export default function Fov({grid, tile, maxRadius, checkTileVisible, canSeeThroughTile, fovArc, targetTile = null}) {

    checkTileVisible = checkTileVisible || function(tile) {
        return true;
    };
    canSeeThroughTile = canSeeThroughTile || function(tile) {
        return false;
    };

    let arc = fovArc || Fov.FOV_ARC_360;
    let fov = [tile];
    let radius = 0;
    let angOfView = AngleOfView(arc);

    maxRadius = maxRadius || false;

    let ring;
    do {
        radius++;
        ring = grid.ring(tile, radius);
        ring.forEach(function(tile2) {
            let angleCorners = tileCenterToCornersRange(tile, tile2);
            let arcCorners = Arc(angleCorners[0], angleCorners[1]);

            if (angOfView.overlaps(arcCorners)) {
                if (checkTileVisible(tile2)) {
                    if (targetTile && targetTile === tile2) {
                        return true;
                    }
                    fov.push(tile2);
                }

                if (!canSeeThroughTile(tile2)) {
                    angOfView.remove(arcCorners);
                }
            }

        });
    } while (
        ring.length > 0 &&
        radius < maxRadius
        );

    return fov;
}

export function unitFov({grid, unit, unitTile = null}) {
    unitTile = unitTile || unit.tile;

    return Fov({
        grid,
        tile: unitTile,
        maxRadius: unit.sightRange,
        canSeeThroughTile(tile) {
            return terrainTransparent(unit, tile);
        },
    });
}

export function unitHasLOSTo({grid, unit, unitTile = null, tile}) {
    unitTile = unitTile || unit.tile;

    let result = Fov({
        grid,
        tile: unitTile,
        maxRadius: unit.sightRange,
        targetTile: tile,
        canSeeThroughTile(tile) {
            return terrainTransparent(unit, tile);
        },
    });

    return result === true;
}

Fov.FOV_ARC_360 = Arc(0, Math.PI * 2);

