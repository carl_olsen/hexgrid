let PI = Math.PI;

export default function Arc(start, end) {
    return Object.create(Arc.prototype).init(start, end);
}

Arc.prototype = {
    init(start, end) {
        let ranges = [];

        if (start >= 0 && end <= 2 * PI) {
            ranges.push([start, end]);
        } else if (start < 0 && end === 0) {
            ranges.push([start + 2 * PI, 2 * PI]);
        } else if (start < 0 && end > 0) {
            ranges.push([start + 2 * PI, 2 * PI]);
            ranges.push([0, end]);
        } else if (end > 2 * PI) {
            ranges.push([start, 2 * PI]);
            ranges.push([0, end - 2 * PI]);
        }

        this.ranges = ranges;
        return this;
    },
    overlaps(arc) {
        let ranges = arc.ranges;

        for (let i = 0; i < ranges.length; i++) {
            if (this.overlapsRange(ranges[i])) {
                return true;
            }
        }
        return false;
    },
    overlapsRange(range) {
        let start = range[0];
        let end = range[1];
        let ranges = this.ranges;

        for (let i = 0; i < ranges.length; i += 1) {
            let min = ranges[i][0];
            let max = ranges[i][1];

            if (
                (start >= min) && (start <= max) ||
                (end >= min) && (end <= max) ||
                (start <= min) && (end >= max)
            ) {
                return true;
            }
        }
        return false;
    },
};

