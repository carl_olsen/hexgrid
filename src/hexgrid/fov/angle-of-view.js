import {degToRad} from '@/hexgrid/util';

export default function(args) {
    return new AngleOfView(args);
};

function AngleOfView(arc) {
    this.ranges = arc.ranges;
}

// Angle of view is a list of ranges of angles that can be seen by a player 
// This is a helper class for the FOV algorithm
AngleOfView.prototype = {
    overlaps(arc) {
        let ranges = this.ranges;

        for (let i = 0; i < ranges.length; i++) {
            if (arc.overlapsRange(ranges[i])) {
                return true;
            }
        }
        return false;
    },
    // remove a arc (hexagon) from the list of visible angles
    remove(arc) {
        arc.ranges.forEach(function(range) {
            this.removeRange(range);
        }, this);
    },
    removeRange: function(range) {
        let i = 0;
        let remove_start = range[0];
        let remove_end = range[1];
        let start;
        let end;
        let new_ranges = [];
        let ranges = this.ranges;
        // Calculates the new ranges
        for (i = 0; i < ranges.length; i++) {
            start = ranges[i][0];
            end = ranges[i][1];

            if (remove_start > end || remove_end < start) {
                new_ranges.push([start, end]);
            } else if (remove_start <= start && remove_end >= end) {
                continue;
            } else if (remove_start <= start && remove_end < end) {
                new_ranges.push([remove_end, end]);
            } else if (remove_start > start && remove_end >= end) {
                new_ranges.push([start, remove_start]);
            } else if (remove_start > start && remove_end < end) {
                new_ranges.push([start, remove_start]);
                new_ranges.push([remove_end, end]);
            }
        }

        // Remove ranges that are too small ( < 1 degree)
        new_ranges = new_ranges.filter(function(range) {
            return (range[1] - range[0]) > degToRad(1);
        });

        this.ranges = new_ranges;
    },
};
