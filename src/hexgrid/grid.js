import Cube from '@/hexgrid/cube';
import Tile from '@/hexgrid/Tile';
import pathfind from '@/_reference/pathfind';
import layouts from '@/hexgrid/layouts';

import {boundsOfPoints, hexagonCornersToSides, hexagonPathCoords, hexagonPathString, lineCenter} from './util';
import {hexToId} from '@/hexgrid/hex';

let SQRT_3_2 = Math.sqrt(3) / 2;
let SQRT_3 = Math.sqrt(3);

export const GRID_LAYOUTS = layouts;

export default function(args) {
    return new Grid(args);
}

function Grid({hexSize, layout, width, height, tileData}) {

    this.maxDistance = 0;

    hexSize = hexSize || 1;
    layout = layout || GRID_LAYOUTS.OddQ;

    this.tiles = new Map();
    this.tilesByCubeId = new Map();

    this.hexSize = hexSize;
    this.layout = layout;
    this.orientation = layout.orientation;
    this.hexToCube = layout.hexToCube;
    this.cubeToHex = layout.cubeToHex;
    this.cubeToHexId = layout.cubeToHexId;

    if (this.orientation) {
        this.hexagonHeight = this.hexSize;
        this.hexagonWidth = SQRT_3_2 * this.hexSize;
    } else {
        this.hexagonHeight = SQRT_3_2 * this.hexSize;
        this.hexagonWidth = this.hexSize;
    }

    this.initTiles(width, height, tileData);

    Object.freeze(this.tilesByCubeId);
    Object.freeze(this.tiles);
}

function makeHexes(width, height) {
    let hexes = [];

    for (let q = 0; q < width; q++) {
        for (let r = 0; r < height; r++) {
            hexes.push({
                q: q,
                r: r,
            });
        }
    }

    return hexes;
}

Grid.prototype = {
    initTiles(width, height, tileData) {

        this.hexagonPathCoords = hexagonPathCoords(this.hexSize, this.orientation);
        let basePxCorners = this.hexagonPathCoords;
        let hexes = makeHexes(width, height);

        hexes.forEach((hex) => {
            let cube = this.hexToCube(hex);

            let pxCenter = this.tileToPxCenter(cube);
            let pxCorners = basePxCorners.map(function(coord) {
                return {
                    x: coord.x + pxCenter.x,
                    y: coord.y + pxCenter.y,
                };
            });

            let pxSides = hexagonCornersToSides(pxCorners);
            let pxSideCenters = pxSides.map(function(line) {
                return lineCenter(line[0], line[1]);
            });

            let id = hexToId(hex);
            let {type, subTypes} = tileData[id];
            let tile = Tile({
                id,
                type,
                subTypes,

                q: hex.q,
                r: hex.r,

                x: cube.x,
                y: cube.y,
                z: cube.z,

                grid: this,

                pxCenter: pxCenter,
                pxCorners: pxCorners,
                pxSides: pxSides,
                pxSideCenters: pxSideCenters,

                hexagonHeight: this.hexagonHeight,
                hexagonWidth: this.hexagonWidth,
            });
            this.tiles.set(tile.id, tile);
            this.tilesByCubeId.set(tile.cubeId, tile);

        });

        this.maxDistance = width + height;
    },

    each(callback, context) {
        context = context || this;
        this.tiles.forEach(callback, context);
        return this;
    },

    map(callback, context) {
        context = context || this;

        return this.toArray().map(callback, context);
    },

    filter(callback, context) {
        context = context || this;

        return this.toArray().filter(callback, context);
    },

    idsToTiles(ids) {
        return ids
            .map((id) => {
                return this.tiles.get(id);
            })
            .filter((tile) => tile);
    },

    get(id) {
        return this.tiles.get(id);
    },

    tileToPxCenter(tile) {
        let x;
        let y;
        if (this.orientation) {
            x = SQRT_3 * tile.x + SQRT_3_2 * tile.z;
            y = 1.5 * tile.z;
        } else {
            x = 1.5 * tile.x;
            y = SQRT_3_2 * tile.x + SQRT_3 * tile.z;
        }
        let half = this.hexSize * 0.5;
        return {
            x: x * half,
            y: y * half,
        };
    },

    pxToTile(p) {
        let half = this.hexSize * 0.5;
        let q, r, x, y, z;

        p.x *= (1 / half);
        p.y *= (1 / half);

        if (this.orientation) {
            q = SQRT_3 / 3 * p.x + -0.333333333333333315 * p.y;
            r = 0.66666666666666663 * p.y;
        } else {
            q = 0.66666666666666663 * p.x;
            r = -0.333333333333333315 * p.x + SQRT_3 / 3 * p.y;
        }

        x = q;
        y = -q - r;
        z = r;
        return this.getByCoords(x, y, z);
    },

    pxBounds() {
        let centers = this.map((tile) => tile.pxCenter);
        let b1 = boundsOfPoints(this.hexagonPathCoords);
        let b2 = boundsOfPoints(centers);
        return {
            minX: b1.minX + b2.minX,
            maxX: b1.maxX + b2.maxX,
            minY: b1.minY + b2.minY,
            maxY: b1.maxY + b2.maxY,
        };
    },

    hexagonPathString() {
        return hexagonPathString(this.hexSize, this.orientation);
    },

    // unused
    pathfindTo(settings) {

        let defaults = {
            startTile: null,
            endTile: null,
            worldSize: this.tiles.size,
            getNeighbors: (tile) => {
                return this.neighbors(tile);
            },
        };

        let s = Object.assign(defaults, settings);

        return pathfind(s);

    },

    neighbors(tile) {
        let cubes = Cube.directions
            .map((nCube) => {
                return Cube.add(tile, nCube);
            });
        return this.cubesToTiles(cubes);
    },

    neighborId(tile, dirIndex) {
        let dir = Cube.directions[dirIndex];
        let cube = Cube.add(tile, dir);
        return this.cubeToHexId(cube);
    },

    neighbor(tile, dirIndex) {
        return this.get(this.neighborId(tile, dirIndex));
    },

    directionOfNeighbor(tile, nTile) {
        for (let i = 0; i < Cube.directions.length; i++) {
            let dir = Cube.directions[i];
            let cube = Cube.add(tile, dir);
            if (cube.cubeId === nTile.cubeId) {
                return i;
            }
        }
    },

    reverseDirection(dir) {
        return Cube.reverseDirection(dir);
    },

    inRadius(tile, radius) {
        let cubes = Cube.inRadius(tile, radius);
        return this.cubesToTiles(cubes);
    },
    distanceTo(fromTile, toTile) {
        return Cube.distance(fromTile, toTile);
    },
    line(a, b) {
        let cubes = Cube.cubeLine(a, b);
        return this.cubesToTiles(cubes);
    },
    ring(tile, distance) {
        let cubes = Cube.ring(tile, distance);
        return this.cubesToTiles(cubes);
    },

    spiral(tile, distance) {
        let cubes = Cube.spiral(tile, distance);
        return this.cubesToTiles(cubes);
    },

    findNearest(tile, findCallback, maxDistance) {
        maxDistance = maxDistance || this.maxDistance;

        let tiles = this.tiles;

        let nearestCube = Cube.findNearest(tile, maxDistance, function(cube) {
            let tile = tiles.get(cube.id);
            if (tile && findCallback(tile)) {
                return tile;
            }
            return false;
        });

        if (nearestCube) {
            return this.tiles.get(nearestCube.id);
        }
    },

    flood(startTile, filterCallback) {

        let selection = [];
        let searched = {};

        let flood = (tile) => {

            if (searched[tile.id]) {
                return;
            }

            searched[tile.id] = true;

            if (!filterCallback(tile)) {
                return;
            }

            selection.push(tile);

            let adj = this.neighbors(tile);
            adj.forEach(flood);
        };

        flood(startTile);

        return selection;

    },

    cubesToTiles(cubes) {
        let tiles = [];
        cubes.forEach((cube) => {
            let tile = this.tilesByCubeId.get(cube.cubeId);
            if (tile) {
                tiles.push(tile);
            }
        });

        return tiles;
    },

    toArray() {
        return [...this.tiles.values()];
    },

    toJson() {
        let {
            maxDistance,
            hexSize,
            layout,
            orientation,
            hexagonHeight,
            hexagonWidth,
        } = this;


        let tiles = Object.fromEntries(this.tiles);
        let tilesByCubeId = Object.fromEntries(this.tiles);

        layout = {
            name: layout.name,
            directionIds: layout.directionIds,
            directionNames: layout.directionNames,
            cornerIds: layout.cornerIds,
            cornerNames: layout.cornerNames,
        };

        return {
            tiles,
            tilesByCubeId,
            maxDistance,
            hexSize,
            layout,
            orientation,
            hexagonHeight,
            hexagonWidth,
        };
    },
};
