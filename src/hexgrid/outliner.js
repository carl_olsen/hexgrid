import round from 'lodash/round';

export function outliner(grid, tiles, startTile) {
    let polyGroups = getPolyGroups(grid, tiles, startTile);
    return polyGroups.map((polyGroup) => {
        return joinLines(polyGroup);
    });
}

function findEdgeTile(grid, tiles) {
    return tiles.find((tile) => {
        let neighbors = grid.neighbors(tile);
        return neighbors.find((nTile) => {
            return !tiles.includes(nTile);
        });
    });
}

function getPolyGroups(grid, tiles, startTile) {
    if (!tiles.length) {
        return [];
    }
    let remainingTiles = new Map();
    tiles.forEach((tile) => remainingTiles.set(tile, true));

    startTile = startTile || findEdgeTile(grid, tiles);
    let lineGroups = [];

    start(startTile);

    return lineGroups;

    function start(startTile) {
        let lines = loop(startTile);
        lines = [...lines.values()];
        lineGroups.push(lines);
        if (remainingTiles.size) {
            let firstKey = remainingTiles.keys().next().value;
            start(firstKey);
        }
    }

    function loop(tile, startIndex = 0, lines = new Map(), loopedTiles = []) {
        loopedTiles.push(tile);
        for (let j = 0; j < 6; j++) {

            let i = j + startIndex;
            if (i > 5) {
                i = i - 6;
            }

            let i2 = i + 1;
            if (i === 5) {
                i2 = 0;
            }

            let nTile = grid.neighbor(tile, i);
            let cornerA = {...tile.pxCorners[i]};
            let cornerB = {...tile.pxCorners[i2]};
            let emptyN = !tiles.includes(nTile);
            cornerA.d = grid.layout.cornerNames[i];
            cornerB.d = grid.layout.cornerNames[i2];

            if (emptyN) {
                let [key, line] = Line(cornerA, cornerB);
                lines.set(key, line);
            } else if (!loopedTiles.includes(nTile)) {
                let rev = grid.reverseDirection(i);
                loop(nTile, rev, lines, loopedTiles);
            }
        }
        remainingTiles.delete(tile);
        return lines;
    }
}

function joinLines(lines) {

    lines = [].concat(lines);
    let polygons = [];
    let max = 100000;
    startPoly();

    function startPoly() {
        let start = lines.shift();
        let poly = [].concat(start);
        polygons.push(poly);

        mapPath(poly);

        if (lines.length) {
            startPoly();
        }
    }

    function mapPath(poly) {
        let end = false;
        let i = 0;
        while (lines.length && i < max && !end) {
            i++;

            let first = poly[0];
            let last = poly[poly.length - 1];
            lines.find((line, i) => {
                let [a, b] = line;

                if (coordsMatch(last, a)) {
                    poly.push(b);
                    lines.splice(i, 1);
                    return true;
                }

                if (coordsMatch(last, b)) {
                    poly.push(a);
                    lines.splice(i, 1);
                    return true;
                }
            });

            if (coordsMatch(first, last)) {
                end = true;
            }
        }
        return poly;
    }


    return polygons;

}

function coordsMatch(a, b) {
    return a.x === b.x && a.y === b.y;
}

function Line(a, b) {
    let ac = Coord(a);
    let bc = Coord(b);
    let nL = normalizeLineCoords([ac, bc]);
    let key = nL[0].x + '_' + nL[0].y + '_' + nL[1].x + '_' + nL[1].y;

    return [key, nL];
}

function Coord({x, y, d}) {
    x = round(x, 3);
    y = round(y, 3);
    return {
        x,
        y,
        d,
    };
}

function normalizeLineCoords(line) {
    line.sort(function(a, b) {
        let o1 = a.x;
        let o2 = b.x;

        let p1 = a.y;
        let p2 = b.y;

        if (o1 != o2) {
            if (o1 < o2) return -1;
            if (o1 > o2) return 1;
            return 0;
        }
        if (p1 < p2) return -1;
        if (p1 > p2) return 1;
        return 0;
    });
    return line;
}
