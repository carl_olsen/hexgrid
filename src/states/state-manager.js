import activationState, {ACTIVATION_STATE} from '@/states/activation-state';
import selectUnitState, {SELECT_UNIT_STATE} from '@/states/select-unit-state';
import emitter from '@/vue/event-bus';
import each from 'lodash/each';

let STATES = {
    [ACTIVATION_STATE]: activationState,
    [SELECT_UNIT_STATE]: selectUnitState,
};

export default function makeGameStateManager({store, grid}) {

    let current;
    let stateManager = {
        set(stateName, args = {}) {
            console.log('state start:', stateName);

            endCurrentState();

            current = makeNewState(stateName);
            current.start(args);
        },
    };

    return stateManager;

    function makeNewState(stateName) {
        let makeNewState = STATES[stateName];
        let newState = makeNewState({
            store,
            stateManager,
            grid,
        });

        each(newState.listeners, (method, key) => {
            emitter.on(key, method);
        });

        return newState;
    }

    function endCurrentState() {
        if (!current) {
            return;
        }
        each(current.listeners, (method, key) => {
            emitter.off(key, method);
        });

        if (current.end) {
            current.end();
        }
    }
}

