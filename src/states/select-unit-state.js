import {ACTIVATION_STATE} from '@/states/activation-state';
import {mapGetters} from 'vuex';

export const SELECT_UNIT_STATE = 'SELECT_UNIT_STATE';

export default function selectUnitState({store, stateManager}) {

    let data = {
        ...mapGetters('units', [
            'unitsByTile',
        ]),
        ...mapGetters('turn', [
            'ownerId',
        ]),
    };

    return {
        listeners: {
            tileMouseEnter(tile) {
                // let unit = store.getters['turn/getFriendlyUnitAtTile'](tile);
                //
                // if (unit) {
                //     let tiles = store.getters['turn/getUnitFovTiles'](unit);
                //     store.dispatch('map/setAllTileDebugTo', {tiles, string: 'FOV'});
                // }
            },
            tileMouseLeave(tile) {
                // store.dispatch('map/clearTileDebug');
            },
            tileClick(tile) {
                let unit = store.getters['turn/getFriendlyUnitAtTile'](tile);
                if (unit) {
                    stateManager.set(ACTIVATION_STATE, {selectUnit: unit});
                }
            },
            clickEndTurn() {
                store.dispatch('turn/newTurn')
                    .then(() => {
                        stateManager.set(SELECT_UNIT_STATE);
                    });
            },
        },
        start({selectUnit}) {
        },
        end() {
        },
    };
}
