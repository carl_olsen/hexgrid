import {isFriendly} from '@/rules/rules';
import {SELECT_UNIT_STATE} from '@/states/select-unit-state';

export const ACTIVATION_STATE = 'ACTIVATION_STATE';

export default function activationState({store, grid, stateManager}) {

    let waitingForMoveAnimation = false;

    let {
        getters,
        commit,
        dispatch,
    } = store;

    return {
        listeners: {
            tileMouseEnter(tile) {
                let enemy = getters['turn/getEnemyUnitAtTile'](tile);
                if (enemy) {
                    let isVisible = getters['turn/getTileIsVisible'](tile);
                    if (isVisible) {
                        commit('selectedUnit/setTargetUnit', enemy);
                        return;
                    }
                }

                commit('selectedUnit/setTargetUnit', null);

                let isValidDest = getters['selectedUnit/validDestTiles'].includes(tile);
                if (isValidDest) {
                    commit('selectedUnit/setDestTile', tile);
                    return;
                }
                commit('selectedUnit/setDestTile', null);
            },
            mapMouseLeave() {
                commit('selectedUnit/setDestTile', null);
                commit('selectedUnit/setTargetUnit', null);
            },
            clickEndTurn() {
                dispatch('turn/newTurn')
                    .then(() => {
                        stateManager.set(SELECT_UNIT_STATE);
                    });
            },
            tileClick: tileMouseClick,
        },
        start({selectUnit}) {
            dispatch('selectedUnit/set', selectUnit);
            // set current tile as destination as unit was likely just clicked
            // and mouse is over that tile
            // also if unit is adjacent to enemy, consider current tile dest
            // when calculating combat
            commit('selectedUnit/setDestTile', selectUnit.tile);
        },
        end() {
            commit('selectedUnit/clear');
        },
    };

    function tileMouseClick(tile) {
        if (waitingForMoveAnimation) {
            return;
        }
        let currentTile = getters['selectedUnit/tile'];
        let selectedUnit = getters['selectedUnit/unit'];

        // deselect unit
        if (tile === currentTile) {
            stateManager.set(SELECT_UNIT_STATE);
            return;
        }
        // move unit
        let isValidDest = getters['selectedUnit/validDestTiles'].includes(tile);
        if (isValidDest) {
            waitingForMoveAnimation = true;
            dispatch('turn/moveUnit', {unit: selectedUnit, tile})
                .then(() => {
                    afterMoveOrAction();
                });
            return;
        }

        // unit at tile
        let unit = getters['units/unitsByTileId'][tile.id];
        if (unit) {
            // select friendly unit
            if (isFriendly(selectedUnit, unit)) {
                stateManager.set(ACTIVATION_STATE, {selectUnit: unit});
                return;
            }

            let isValidAttack = getters['turn/getIsValidAttack'](selectedUnit, unit);

            // select enemy unit
            if (isValidAttack) {
                dispatch('turn/resolveCombat', {
                    source: selectedUnit,
                    target: unit,
                }).then(() => {
                    afterMoveOrAction();
                });
            }
            return;
        }

        // deselect unit
        stateManager.set(SELECT_UNIT_STATE);
    }

    function afterMoveOrAction() {
        let canAct = getters['selectedUnit/canAct'];
        if (canAct) {
            let unit = getters['selectedUnit/unit'];
            stateManager.set(ACTIVATION_STATE, {selectUnit: unit});
            return;
        }

        stateManager.set(SELECT_UNIT_STATE);
    }
}
