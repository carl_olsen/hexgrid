import mitt from 'mitt';

const emitter = makeEmitter();

export default emitter;
export const animations = makeAnimations();

function makeEmitter() {
    const emitter = mitt();
    emitter.once = (type, handler) => {
        const wrappedHandler = (evt) => {
            handler(evt);
            emitter.off(type, wrappedHandler);
        };
        emitter.on(type, wrappedHandler);
    };

    return emitter;
}

let animationIdIncrement = 0;

function makeAnimations() {
    const mitt = makeEmitter();
    const eventPrefix = 'animationComplete.';
    return {

        moveUnit: makeCycle({
            mitt,
            startEvent: 'unitMoveStart',
            completeEvent: 'unitMoveComplete',
        }),

        unitAttack: makeCycle({
            mitt,
            startEvent: 'unitAttackStart',
            completeEvent: 'unitAttackComplete',
        }),

        damage: makeCycle({
            mitt,
            startEvent: 'damageStart',
            completeEvent: 'damageComplete',
        }),
    };
}

function makeCycle({mitt, startEvent, completeEvent}) {
    return {
        start(args = {}) {
            let animationId = animationIdIncrement++;

            let promise = new Promise((resolve) => {
                // resolve promise when complete event emitted
                mitt.on(completeEvent + animationId, resolve);
            });

            args.animationId = animationId;
            mitt.emit(startEvent, args);

            return promise;
        },
        onStart(callback) {
            mitt.on(startEvent, callback);
        },
        complete(animationId) {
            mitt.emit(completeEvent + animationId, animationId);
        },
    };
}
