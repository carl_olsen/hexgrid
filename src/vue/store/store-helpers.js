
export function mapDefaultMutations(mutations) {
    const out = {}

    Object.keys(mutations).forEach((mutation) => {
        const property = mutations[mutation]

        out[mutation] = (state, payload) => {
            state[property] = payload
        }
    })

    return out
}

export function mapDefaultGetters(array) {
    const out = {}

    array.forEach((key) => {
        out[key] = (state) => state[key]
    })

    return out
}

export function mapDefaultActions(array) {
    const out = {}

    array.forEach((key) => {
        out[key] = ({ commit }, payload) => {
            commit(key, payload)

            return Promise.resolve()
        }
    })

    return out
}

export function mapRoot(commitOrDispatch) {
    return (action, payload, options = {}) => {
        options.root = true

        return commitOrDispatch(action, payload, options)
    }
}
