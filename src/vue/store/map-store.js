import Vue from 'vue';
import Vuex from 'vuex';
import {styleConfig} from '@/config';
import {mapDefaultActions, mapDefaultGetters, mapDefaultMutations} from '@/vue/store/store-helpers';
import each from 'lodash/each';


Vue.use(Vuex);

let {mapMargin} = styleConfig;

export default function makeMapStore({grid}) {

    let {minX, maxX, minY, maxY} = grid.pxBounds();
    let boundsWidth = maxX - minX;
    let boundsHeight = maxY - minY;
    let width = boundsWidth + mapMargin * 2;
    let height = boundsHeight + mapMargin * 2;

    let corners = grid.toArray()[0].pxCorners;
    let nwCorner = corners[4];
    let neCorner = corners[5];
    let hexagonSideLength = neCorner.x - nwCorner.x;

    return {
        namespaced: true,
        strict: process.env.NODE_ENV !== 'production',

        state() {
            return {
                bounds: {
                    minX,
                    maxX,
                    minY,
                    maxY,
                },
                boundsWidth,
                boundsHeight,
                width,
                height,
                hexagonHeight: grid.hexagonHeight,
                hexagonSideLength,
                hoveredTile: null,
                tileDebugData: {},
            };
        },
        mutations: {
            ...mapDefaultMutations({
                setHoveredTile: 'hoveredTile',
            }),
            setTileDebug(state, {tile, string}) {
                Vue.set(state.tileDebugData, tile.id, string);
            },
            clearTileDebug(state) {
                state.tileDebugData = {};
            },
            setAllTileDebug(state, tileDebug) {
                state.tileDebugData = {};
                each(tileDebug, (string, tileId) => {
                    Vue.set(state.tileDebugData, tileId, string);
                });
            },
        },
        actions: {
            ...mapDefaultActions([
                'clearTileDebug',
                'setTileDebug',
                'setAllTileDebug',
            ]),
            setAllTileDebugTo({commit}, {tiles, string}) {
                let debugTileData = {};
                tiles.forEach((tile) => {
                    debugTileData[tile.id] = string;
                });

                commit('setAllTileDebug', debugTileData);
            },
        },
        getters: {
            ...mapDefaultGetters([
                'boundsHeight',
                'boundsWidth',
                'hexagonHeight',
                'hoveredTile',
                'hexagonSideLength',
                'tileDebugData',
            ]),
            grid() {
                return grid;
            },
            tiles() {
                return grid.toArray();
            },
            tilesById() {
                return grid.tilesById();
            },
            hoveredTileId(state) {
                if (state.hoveredTile) {
                    return state.hoveredTile.id;
                }
            },
        },
    };
}
