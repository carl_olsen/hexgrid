import {mapDefaultGetters} from '@/vue/store/store-helpers';
import Unit from '@/game/unit';
import Vue from 'vue';

let idIncrement = 0;

export default function makeUnitsStore({grid}) {
    return {
        namespaced: true,
        strict: process.env.NODE_ENV !== 'production',

        state() {
            return {
                unitsByTileId: {},
                unitsById: {},
            };
        },
        mutations: {
            set(state, {unit, tile}) {
                unit.id = idIncrement++;
                unit.tile = tile;
                Vue.set(state.unitsById, unit.id, unit);
                Vue.set(state.unitsByTileId, tile.id, unit);
            },
            setAll(state, unitsData) {
                let unitTiles = unitsData.map(({key, tileId, ownerId}) => {
                    let tile = grid.get(tileId);
                    if (!tile) {
                        throw new Error('tile not found ' + tileId);
                    }

                    let unit = Unit({
                        ownerId,
                        key,
                    });
                    return {
                        unit,
                        tile,
                    };
                });

                unitTiles.forEach(({unit, tile}) => {
                    unit.id = idIncrement++;
                    unit.tile = tile;
                    Vue.set(state.unitsById, unit.id, unit);
                    Vue.set(state.unitsByTileId, tile.id, unit);
                });
            },
            move(state, {unit, tile}) {
                if (unit.tile) {
                    Vue.delete(state.unitsByTileId, unit.tile.id);
                }
                unit.tile = tile;
                Vue.set(state.unitsByTileId, tile.id, unit);
            },
            delete(state, unit) {
                Vue.delete(state.unitsById, unit.id);
                Vue.delete(state.unitsByTileId, tile.id);
            },
            damage(state, {unit, amount}) {
                let target = state.unitsById[unit.id];
                target.hp -= amount;
            },
            setHealth(state, {unit, amount}) {
                let target = state.unitsById[unit.id];
                target.hp = Math.min(amount, target.hpMax);
            },
        },
        getters: {
            ...mapDefaultGetters([
                'unitsByTileId',
            ]),
            units(state) {
                return Object.values(state.unitsById);
            },
            filter(state, getters) {
                return (callback) => {
                    return getters.units.filter(callback);

                };
            },
            find(state, getters) {
                return (callback) => {
                    return getters.units.find(callback);

                };
            },
            getFriendlyUnits(state, getters) {
                return (ownerId) => {
                    return getters.units.filter((unit) => {
                        return unit.ownerId === ownerId;
                    });
                };
            },
        },
    };
}
