import {mapDefaultGetters, mapRoot} from '@/vue/store/store-helpers';
import {unitFov} from '@/hexgrid/fov';
import getMovePaths from '@/game/unit/unit-pathfinder';
import {isValidTarget} from '@/rules/rules';
import Combat from '@/rules/combat';
import {animations} from '@/vue/event-bus';
import Vue from 'vue';

export default function makeTurnStore({grid}) {
    return {
        namespaced: true,
        strict: process.env.NODE_ENV !== 'production',

        state() {
            return {
                ownerId: null,
                units: [],
                unitMoves: {},
                unitActions: {},
                unitFovs: {},
                unitPaths: {},
            };
        },
        mutations: {
            newTurn(state, {ownerId, units}) {
                state.ownerId = ownerId;
                state.units = [].concat(units);
                state.unitMoves = {};
                state.unitActions = {};
                state.unitFovs = {};
                state.unitPaths = {};

                units.forEach((unit) => {
                    Vue.set(state.unitMoves, unit.id, unit.move);
                    Vue.set(state.unitActions, unit.id, [].concat(unit.actions));

                    let fov = unitFov({
                        grid,
                        unit,
                    });
                    Vue.set(state.unitFovs, unit.id, fov);
                });
            },
            spendUnitMoves(state, {unit, amount}) {
                let moves = state.unitMoves[unit.id];
                if (!(typeof moves === 'number')) {
                    throw new Error('trying to get unit moves when not set');
                }

                if (amount > moves) {
                    throw new Error('trying to sub ' + amount + ' moves when unit only has ' + moves);
                }

                let newVal = moves - amount;
                if (newVal) {
                    Vue.set(state.unitMoves, unit.id, newVal);
                } else {
                    Vue.set(state.unitMoves, unit.id, 0);
                }
            },
            spendRemainingUnitMoves(state, unit) {
                Vue.set(state.unitMoves, unit.id, 0);
            },
            spendAction(state, {unit, action}) {
                let actions = state.unitActions[unit.id];

                if (!actions.includes(action)) {
                    throw new Error('trying to spend ' + action + ' action when unit does not have it');
                }
                let index = actions.indexOf(action);
                actions.splice(index, 1);
            },
            setUnitPathData(state, {unit, pathData}) {
                Vue.set(state.unitPaths, unit.id, pathData);
            },
            recalculateUnitFov(state, {unit, tile}) {
                let fov = unitFov({
                    grid,
                    unit,
                    unitTile: tile,
                });
                Vue.set(state.unitFovs, unit.id, fov);
            },
            resetUnitPaths(state) {
                state.unitPaths = {};
            },
        },
        actions: {
            newTurn({state, commit, getters, rootGetters}, ownerId = null) {
                let commitRoot = mapRoot(commit);

                if (!ownerId) {
                    ownerId = getters.nextOwnerId;
                }
                commitRoot('selectedUnit/clear');

                let units = rootGetters['units/getFriendlyUnits'](ownerId);
                commit('newTurn', {ownerId, units});
            },
            moveUnit({state, commit, dispatch, getters}, {unit, tile}) {
                let commitRoot = mapRoot(commit);

                return dispatch('getUnitPaths', unit)
                    .then(({paths, pathDebug}) => {
                        if (!paths[tile.id]) {
                            throw new Error('attempting to start animate to invalid destination');
                        }
                        let {path, cost} = paths[tile.id];

                        let tiles = path.slice(1);
                        return animations.moveUnit.start({unit, tiles})
                            .then((animationId) => {
                                commitRoot('units/move', {unit, tile});
                                commit('spendUnitMoves', {unit, amount: cost});
                                commit('resetUnitPaths');
                                commit('recalculateUnitFov', {unit, tile: unit.tile});
                                return dispatch('getUnitPaths', unit);
                            });
                    });
            },
            getUnitPaths({state, commit, rootGetters}, unit) {
                if (!state.unitPaths[unit.id]) {
                    let movesLeft = state.unitMoves[unit.id];
                    let pathData;
                    if (movesLeft) {
                        let unitsByTileId = rootGetters['units/unitsByTileId'];
                        let {paths, pathDebug} = getMovePaths(grid, unitsByTileId, unit, movesLeft);

                        pathData = Object.freeze({paths, pathDebug});
                    } else {
                        pathData = {paths: {}, pathDebug: null};
                    }

                    commit('setUnitPathData', {unit, pathData});
                }
                return state.unitPaths[unit.id];
            },
            resolveCombat({state, getters, dispatch, commit}, {source, target}) {
                let commitRoot = mapRoot(commit);

                commit('spendAction', {
                    unit: source,
                    action: 'attack',
                });

                let combat = Combat({
                    grid,
                    source: source,
                    target: target,
                });

                const result = combat.getResult();

                let {
                    damageToTarget,
                    damageToSource,
                } = result;

                commitRoot('units/damage', {unit: target, amount: damageToTarget});
                commitRoot('units/damage', {unit: source, amount: damageToSource});

                animations.damage.start({
                    tile: target.tile,
                    damage: damageToTarget,
                });

                animations.damage.start({
                    tile: source.tile,
                    damage: damageToSource,
                });

                animations.unitAttack.start({
                    unit: source,
                    tile: target.tile,
                });

                commit('spendRemainingUnitMoves', source);
            },
        },
        getters: {
            ...mapDefaultGetters([
                'unitsByTileId',
                'ownerId',
                'debugTileData',
            ]),
            unitsThatCanAct(state, getters, rootState, rootGetters) {
                return state.units.filter((unit) => {
                    return getters.getUnitCanAct(unit);
                });
            },
            visibleTiles(state) {
                let visibleTiles = [];

                state.units.forEach(unit => {
                    visibleTiles.push(unit.tile);
                });

                Object.values(state.unitFovs)
                    .forEach((fovTiles) => {
                        fovTiles.forEach((tile) => {
                            if (!visibleTiles.includes(tile)) {
                                visibleTiles.push(tile);
                            }
                        });
                    });
                Object.freeze(visibleTiles);

                return visibleTiles;
            },
            nextOwnerId(state, getters, rootState, rootGetters) {
                let {
                    player1Id,
                    player2Id,
                } = rootGetters;

                let currentId = state.ownerId;
                if (currentId) {
                    if (currentId === player1Id) {
                        return player2Id;
                    } else {
                        return player1Id;
                    }
                }
                return player1Id;
            },
            getUnitMovesLeft(state) {
                return (unit) => {
                    return state.unitMoves[unit.id];
                };
            },
            getUnitActions(state) {
                return (unit) => {
                    return state.unitActions[unit.id] || [];
                };
            },
            getHasAction(state) {
                return (unit, type) => {
                    let actions = state.unitActions[unit.id] || [];
                    return !!actions.find((act) => type === act);
                };
            },
            getUnitHasActionsLeft(state, getters) {
                return (unit) => {
                    let actions = state.unitActions[unit.id] || [];
                    return !!(actions && actions.length);
                };
            },
            getUnitCanAct(state, getters) {
                return (unit) => {
                    let movesLeft = getters.getUnitMovesLeft(unit);
                    if (movesLeft) {
                        return true;
                    }
                    let actionsLeft = getters.getUnitHasActionsLeft(unit);
                    if (!actionsLeft) {
                        return false;
                    }
                    let validTargetTiles = getters.getUnitValidTargetTiles(unit);
                    let hasValidTargets = validTargetTiles && validTargetTiles.length;

                    return hasValidTargets;
                };
            },
            getUnitPathing(state) {
                return (unit) => {
                    let result = state.unitPaths[unit.id];
                    if (!result) {
                        throw new Error('attempting to get pathing for unit without calling dispatch("getUnitPaths", unit) first');
                    }
                    return result;
                };
            },
            getUnitValidTargetTiles(state, getters, rootState, rootGetters) {
                return (unit, unitTile = null) => {

                    unitTile = unitTile || unit.tile;

                    if (!getters.getHasAction(unit, 'attack')) {
                        return [];
                    }

                    let tilesInRange = grid.inRadius(unitTile, unit.attackRange);

                    let fovTiles = unitFov({
                        grid,
                        unit,
                        unitTile,
                    });

                    let unitsByTileId = rootGetters['units/unitsByTileId'];

                    return tilesInRange.filter((tile) => {
                        if (!fovTiles.includes(tile)) {
                            return false;
                        }
                        let target = unitsByTileId[tile.id];
                        if (!target) {
                            return false;
                        }

                        return isValidTarget(grid, unit, target, unitTile);
                    });
                };
            },
            getIsValidAttack(state, getters) {
                return (source, target) => {
                    let hasAttackAction = getters.getHasAction(source, 'attack');
                    if (!hasAttackAction) {
                        return false;
                    }

                    let fovTiles = getters.getUnitFovTiles(source);
                    let lasLOS = fovTiles.includes(target.tile);
                    if (!lasLOS) {
                        return false;
                    }

                    return isValidTarget(grid, source, target);
                };
            },
            getFriendlyUnitAtTile(state, getters, rootState, rootGetters) {
                return (tile) => {
                    let unit = rootGetters['units/unitsByTileId'][tile.id];
                    let isFriendly = unit && unit.ownerId === state.ownerId;
                    if (isFriendly) {
                        return unit;
                    }
                };
            },
            getEnemyUnitAtTile(state, getters, rootState, rootGetters) {
                return (tile) => {
                    let unit = rootGetters['units/unitsByTileId'][tile.id];
                    let isEnemy = unit && unit.ownerId !== state.ownerId;
                    if (isEnemy) {
                        return unit;
                    }
                };
            },
            getUnitFovTiles(state) {
                return (unit) => {
                    return state.unitFovs[unit.id];
                };
            },
            getTileIsVisible(state, getters) {
                return (tile) => {
                    return getters.visibleTiles.includes(tile);
                };
            },
        },
    };
}
