import {mapDefaultGetters, mapDefaultMutations, mapRoot} from '@/vue/store/store-helpers';
import {terrainCost, terrainDefense} from '@/rules/terrain';
import Combat from '@/rules/combat';

export default function makeSelectedUnitStore({grid}) {
    return {
        namespaced: true,
        strict: process.env.NODE_ENV !== 'production',

        state() {
            return {
                unit: null,
                destTile: null,
                targetUnit: null,
            };
        },
        mutations: {
            ...mapDefaultMutations({
                set: 'unit',
                setDestTile: 'destTile',
                setTargetUnit: 'targetUnit',
            }),
            clear(state) {
                state.unit = null;
                state.destTile = null;
                state.targetUnit = null;
            },
        },
        actions: {
            set({state, commit, dispatch}, unit) {
                let dispatchRoot = mapRoot(dispatch);

                if (unit) {
                    return dispatchRoot('turn/getUnitPaths', unit)
                        .then(() => {
                            commit('set', unit);
                        });
                }
                commit('set', unit);
            },
        },
        getters: {
            ...mapDefaultGetters([
                'unit',
                'destTile',
                'targetUnit',
            ]),
            tile(state) {
                if (!state.unit) {
                    return;
                }
                return state.unit.tile;
            },
            destTilePathData(state, getters, rootState, rootGetters) {
                let hoveredTile = getters.destTile;
                let unit = state.unit;
                let result;
                if (unit && hoveredTile) {
                    let {paths} = rootGetters['turn/getUnitPathing'](unit);
                    result = paths[hoveredTile.id];
                }
                return result || {cost: null, endTile: null, path: []};
            },
            validDestTiles(state, getters, rootState, rootGetters) {
                let unit = state.unit;
                if (!unit) {
                    return [];
                }
                let {paths} = rootGetters['turn/getUnitPathing'](unit);
                return Object.values(paths).map(({endTile}) => endTile);
            },
            validTargetTilesFromDest(state, getters, rootState, rootGetters) {
                let unit = state.unit;
                if (!unit) {
                    return [];
                }

                let destTile = state.destTile;

                return rootGetters['turn/getUnitValidTargetTiles'](unit, destTile);
            },
            canAct(state, getters, rootState, rootGetters) {
                return rootGetters['turn/unitsThatCanAct'].includes(state.unit);
            },
            destTileInfo(state, getters, rootState, rootGetters) {
                let unit = state.unit;
                if (!unit) {
                    return;
                }

                let tile = getters.destTile;
                if (!tile) {
                    return;
                }

                let {cost, endTile, path} = getters.destTilePathData;
                if (!path.length) {
                    return;
                }
                let tileBeforeLast = path[path.length - 2];

                return {
                    type: tile.type,
                    subTypes: tile.subTypes || [],
                    terrainDefense: terrainDefense(unit, tile),
                    moveCost: terrainCost(unit, tile, tileBeforeLast),
                    pathCost: cost,
                };
            },
            unitInfo(state, getters, rootState, rootGetters) {
                let unit = state.unit;
                if (!unit) {
                    return;
                }
                return {
                    ...unit,
                    unitMoves: rootGetters['turn/getUnitMovesLeft'](unit),
                };
            },
            hoveredUnitOdds(state, getters) {
                let source = state.unit;
                let destTile = state.destTile;
                let target = state.targetUnit;

                if (!source || !target) {
                    return;
                }

                let isValidAttackFromDestTile = getters.validTargetTilesFromDest.includes(target.tile);
                if (!isValidAttackFromDestTile) {
                    return;
                }

                let combat = Combat({
                    grid,
                    source,
                    sourceTile: destTile,
                    useSourceTerrain: isValidAttackFromDestTile,
                    target,
                });

                return combat.getData();
            },
        },
    };
}
