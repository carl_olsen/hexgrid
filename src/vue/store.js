import Vue from 'vue';
import Vuex from 'vuex';
import makeUnitsStore from '@/vue/store/units-store';
import makeTurnStore from '@/vue/store/turn-store';
import makeMapStore from '@/vue/store/map-store';
import {mapDefaultGetters} from '@/vue/store/store-helpers';
import makeSelectedUnitStore from '@/vue/store/selected-unit-store';

Vue.use(Vuex);

export default function makeStore({grid}) {

    return new Vuex.Store({
        strict: process.env.NODE_ENV !== 'production',

        modules: {
            units: makeUnitsStore({grid}),
            turn: makeTurnStore({grid}),
            map: makeMapStore({grid}),
            selectedUnit: makeSelectedUnitStore({grid}),
        },
        state() {
            return {
                player1Id: 'player1',
                player2Id: 'player2',
            };
        },
        actions: {},
        getters: {
            ...mapDefaultGetters([
                'player1Id',
                'player2Id',
            ]),
            grid() {
                return grid;
            },
        },
    });
}
