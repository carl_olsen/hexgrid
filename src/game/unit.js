import units from '@/../data/units';
import cloneDeep from 'lodash/cloneDeep';

export default function(args) {
    return new Unit(args);
};

function Unit({ownerId, key}) {

    let unitData = units[key];

    this.ownerId = ownerId;
    this.key = unitData.key;
    this.name = unitData.name;
    this.hp = unitData.hpMax;
    this.hpMax = unitData.hpMax;
    this.attackRange = unitData.attackRange;
    this.attackMinRange = unitData.attackMinRange;
    this.move = unitData.move;
    this.shortName = unitData.shortName;
    this.type = unitData.type;
    this.sightRange = unitData.sightRange;

    let {
        natoSymbols,
        actions,
        stats,
    } = unitData;

    Object.assign(
        this,
        cloneDeep({
            natoSymbols,
            actions,
            stats,
        }),
    );
}
