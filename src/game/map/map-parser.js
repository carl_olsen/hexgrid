import Grid, {GRID_LAYOUTS} from '@/hexgrid/grid';
import {styleConfig} from '@/config';

export function loadMap(mapData) {
    let {staggeraxis} = mapData;
    let layout;
    if (staggeraxis === 'x') {
        layout = GRID_LAYOUTS.OddQ;
    }

    let {
        tileData,
        unitData,
        width,
        height,
    } = parseMap(mapData);

    let grid = Grid({
        hexSize: styleConfig.hexSize,
        tileData,
        layout,
        width,
        height,
    });


    let unitsData = [];
    for (let tileId in unitData) {
        let unit = unitData[tileId];
        unitsData.push({
            tileId,
            key: unit.unitKey,
            ownerId: unit.ownerId,
        });
    }

    return {
        unitsData,
        grid,
    };
}

export function parseMap(mapData) {
    let width = mapData.width;
    let height = mapData.height;
    let tileSet = mapData.tilesets.find((tileset) => tileset.name === 'main');
    let tileData = {};
    let unitData = {};
    let tileProperties = parseTileSetProperties(tileSet);

    mapData.layers.forEach((layer) => {

        if (layer.type !== 'tilelayer') {
            return;
        }

        let rows = mapDataToRows(layer.data, width);
        let rowsData = parseRows(rows, tileProperties);

        for (let key in rowsData) {

            let item = rowsData[key];

            let type = item.type;
            let subType = item.subType;
            let unitKey = item.unitKey;
            let ownerId = item.ownerId;

            if (!tileData[key]) {
                tileData[key] = {
                    subTypes: [],
                };
            }

            if (unitKey && !unitData[key]) {
                unitData[key] = {
                    x: item.x,
                    y: item.y,
                    unitKey,
                    ownerId,
                };
            }

            if (type) {
                tileData[key].type = type;
            }

            if (subType) {
                tileData[key].subTypes.push(subType);
            }

        }

    });

    return {
        width,
        height,
        tileData,
        unitData,
    };
}

function mapDataToRows(data, width) {
    let rows = [];
    let chunk = width;

    for (let i = 0; i < data.length; i += chunk) {
        let rowChunk = data.slice(i, i + chunk);
        rows.push(rowChunk);
    }
    return rows;
}

function parseRows(rows, tileProperties) {
    let parsedData = {};
    for (let y = 0; y < rows.length; y++) {
        let row = rows[y];

        for (let x = 0; x < row.length; x++) {

            let key = x + ',' + y;
            let tilePropertyIndex = row[x] - 1;
            let tile = {
                x,
                y,
            };
            parsedData[key] = Object.assign(tile, tileProperties[tilePropertyIndex]);
        }
    }
    return parsedData;
}

function parseTileSetProperties(tileSet) {
    let tileProperties = {};
    tileSet.tiles.forEach(({id, properties}) => {
        tileProperties[id] = parseTileProperties(properties);
    });
    return tileProperties;
}

function parseTileProperties(properties) {
    let parsed = {};
    properties.forEach(({name, type, value}) => {
        parsed[name] = value;
    });
    return parsed;
}
