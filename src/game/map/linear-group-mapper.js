
let validSubTypes = ['river', 'road', 'dirt_road', 'trail'];

export default function mapLinearGroups(grid) {
    let subTypes = getSubTypes(grid);
    let groups   = [];

    subTypes.forEach((subType) => {
        let tiles = grid.filter((tile) => {
            return tile.subTypes.includes(subType);
        });
        let subGroups = groupByFlood(grid, tiles);

        groups.push({
                    subType,
            groups: subGroups,
        })
    });

    return groups;
}

function getSubTypes(grid) {
    let subTypes = {};
    grid.each((tile) => {
        if (tile.subTypes) {
            tile.subTypes.forEach((st) => {

                if(validSubTypes.includes(st)){
                    subTypes[st] = true;
                }
            });
        }
    });
    return Object.keys(subTypes);
}

function groupByFlood(grid, tiles) {
    let groups = [];
    let mapped = {};

    tiles.forEach((tile) => {

        if (mapped[tile.id]) {
            return;
        }

        let group = grid.flood(tile, function(tile) {
            if (mapped[tile.id]) {
                return false;
            }
            if (tiles.includes(tile)) {
                mapped[tile.id] = true;
                return true;
            }
        });
        groups.push(group);
    });

    return groups;
}
