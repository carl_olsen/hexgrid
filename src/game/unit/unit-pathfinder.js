import {DEBUG_PATHS} from '@/config';
import {terrainCost} from '@/rules/terrain';
import {checkTileMovement} from '@/rules/rules';

export default function getMovePaths(grid, unitsByTile, unit, maxDistance) {
    const unreachableTiles = [];
    const invalidDestTiles = [];
    const trackedCosts = new Map();
    const trackedParents = new Map();
    const startTile = unit.tile;
    const pendingTiles = [];

    maxDistance += 1;
    getAdjacentWithCosts(startTile).forEach((moveCost, neighbor) => {
        trackedParents.set(neighbor, startTile);
        trackedCosts.set(startTile, moveCost);
        pendingTiles.push(startTile);
    });

    function getAdjacentWithCosts(tile) {
        let neighborMoveCosts = new Map();

        grid.neighbors(tile).forEach((neighbor) => {
            if (unreachableTiles.includes(neighbor)) {
                return;
            }
            let {canMoveThrough, canOccupy} = checkTileMovement(unitsByTile, unit, neighbor);

            if(!canMoveThrough && !canOccupy){
                unreachableTiles.push(neighbor);
                return;
            }

            if (canMoveThrough) {
                let moveCost = terrainCost(unit, tile, neighbor);

                neighborMoveCosts.set(neighbor, moveCost);

                if (!canOccupy) {
                    invalidDestTiles.push(neighbor);
                }
            }
        });
        return neighborMoveCosts;
    }

    let tile = pendingTiles.shift();
    while (tile) {
        let costToTile = trackedCosts.get(tile);

        getAdjacentWithCosts(tile)
            .forEach((costToAdjacent, child) => {
                let totalCost = costToTile + costToAdjacent;
                if (totalCost <= maxDistance) {
                    let childCost = trackedCosts.get(child);
                    if (!childCost || childCost > totalCost) {
                        trackedCosts.set(child, totalCost);
                        trackedParents.set(child, tile);
                        pendingTiles.push(child);
                    }
                }
            });

        tile = pendingTiles.shift();
    }

    let result = {};
    let freeze = Object.freeze;
    trackedCosts.forEach((cost, tile) => {
        let path = getOptimalPath(tile, trackedParents);
        freeze(path)
        if (unreachableTiles.includes(tile)) {
            return;
        }

        if (invalidDestTiles.includes(tile)) {
            return;
        }

        result[tile.id] = freeze({
            endTile: tile,
            cost: cost - 1,
            path,
        });
    });
    freeze(result);

    if (DEBUG_PATHS) {
        trackedCosts.forEach((cost, tile) => {
            trackedCosts.set(tile, cost - 1);
        });
    }

    let pathDebug = false;
    if (DEBUG_PATHS) {
        pathDebug = {
            unreachableTiles,
            trackedCosts,
            trackedParents,
        };
    }

    return {
        paths: result,
        pathDebug,
    };
}

function getOptimalPath(dest, trackedParents) {
    let optimalPath = [dest];
    let parent = trackedParents.get(dest);
    while (parent) {
        optimalPath.push(parent);
        parent = trackedParents.get(parent);
    }
    optimalPath.reverse();
    return optimalPath;
}
