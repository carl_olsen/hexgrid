import {CombatStat, Stat, TerrainDefenseMod} from '@/rules/mods';
import {rollDamage} from '@/rules/combat-roll';
import {isValidTarget} from '@/rules/rules';

export default function Combat({
                                   grid,
                                   source,
                                   sourceTile = null,
                                   target,
                                   targetTile = null,
                                   useSourceTerrain = true,
                                   useTargetTerrain = true,
                               }) {

    sourceTile = sourceTile || source.tile;
    targetTile = targetTile || target.tile;

    return {
        getData,
        getResult,
    };

    function getResult() {
        const data = getData();

        let sourceRoll = rollDamage(data.sourceCombatValue);
        let targetRoll = rollDamage(data.targetCombatValue);

        data.damageToTarget = sourceRoll.damage;
        data.damageToSource = targetRoll.damage;

        console.log('sourceRoll', {
            roll: sourceRoll.roll,
            rollDamage: sourceRoll.damage,
            combatValueBaseRaw: data.sourceCombatValueRaw,
            combatValueBase: data.sourceCombatValue,
            dmgOut: data.damageToTarget,
        });

        console.log('targetRoll', {
            roll: targetRoll.roll,
            rollDamage: targetRoll.damage,
            combatValueBaseRaw: data.targetCombatValueRaw,
            combatValueBase: data.targetCombatValue,
            dmgOut: data.damageToSource,
        });

        return data;
    }

    function getData() {
        const sourceAtk = CombatStat(true, 'attack', source, target);
        const sourceDef = CombatStat(true, 'defense', source, target);
        let sourceHpPercent = sourceAtk.hpPercent;

        if (useSourceTerrain) {
            const sourceTerrainMod = TerrainDefenseMod(source, sourceTile);
            if (sourceTerrainMod) {
                sourceDef.addMod(sourceTerrainMod);
            }
        }

        let targetAtk;
        if (isValidTarget(grid, target, source, targetTile, sourceTile)) {
            targetAtk = CombatStat(false, 'attack', target, source);
        } else {
            targetAtk = Stat('attack', 0);
        }

        const targetDef = CombatStat(false, 'defense', target, source);
        let targetHpPercent = targetAtk.hpPercent;
        if (useTargetTerrain) {
            const targetTerrainMod = TerrainDefenseMod(target, targetTile);
            if (targetTerrainMod) {
                targetDef.addMod(targetTerrainMod);
            }
        }

        const sourceCombatValueRaw = targetAtk.finalValueRaw - sourceDef.finalValueRaw;
        const targetCombatValueRaw = sourceAtk.finalValueRaw - targetDef.finalValueRaw;

        const sourceCombatValue = Math.round(sourceCombatValueRaw);
        const targetCombatValue = Math.round(targetCombatValueRaw);

        return {
            sourceId: source.id,
            sourceName: source.name,

            source,
            sourceHp: source.hp,
            sourceHpPercent,
            sourceAtk,
            sourceDef,
            sourceCombatValue,
            sourceCombatValueRaw,

            targetId: target.id,
            targetName: target.name,
            target,
            targetHp: target.hp,
            targetHpPercent,
            targetAtk,
            targetDef,
            targetCombatValue,
            targetCombatValueRaw,
        };
    }
};
