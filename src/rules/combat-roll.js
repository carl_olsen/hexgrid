import mapValues from 'lodash/mapValues';
import each from 'lodash/each';

export const valueToMaxDamage = {
    '-5': 1,
    '-4': 2,
    '-3': 3,
    '-2': 4,
    '-1': 5,
    0: 6,
    1: 7,
    2: 8,
    3: 9,
    4: 10,
    5: 11,
    6: 12,
    7: 13,
    8: 14,
    9: 15,
    10: 16,
    11: 17,
    12: 18,
};

const minCombatValue = Math.min(...Object.keys(valueToMaxDamage));

let combatDistribution = Object.freeze(calcDistribution(valueToMaxDamage));

export function allChances(targetHp = null) {
    let values = getSortedIntKeys(combatDistribution);
    return values.map((value) => {

        let chances = getCombatChances(value, targetHp);
        return {
            value,
            chances,
        };
    });
}

export function getCombatChances(combatValue, targetHp = null) {
    if (combatValue < minCombatValue) {
        combatValue = minCombatValue;
    }
    let chances = Object.values(combatDistribution[combatValue]);

    if (!targetHp) {
        return chances;
    }

    let killPercent = 0;

    let output = chances.filter(({damage, chance, percent}) => {
        let valid = damage < targetHp;

        if (!valid) {
            killPercent += percent;
        }
        return valid;
    });

    if (killPercent) {
        output.push({
            percent: killPercent,
            chance: killPercent,
            damage: targetHp,
            kill: true
        });
    }

    return output;
}

export function rollDamage(value) {
    let roll = Math.random() * 100;

    let chances = getCombatChances(value);

    let current = 0;
    let item;
    for (let i = 0; i < chances.length; i++) {

        item = chances[i];
        current += item.percent;
        if (roll <= current) {
            return {
                roll,
                damage: item.damage,
            };
        }
    }

    return {
        roll,
        damage: item.damage,
    };
}

function calcDistribution(valueToMaxDamage) {

    let values = getSortedIntKeys(valueToMaxDamage);

    let absMaxDamage = Math.max(...Object.values(valueToMaxDamage));
    let distribution = {};

    values.forEach(value => {
        let maxDamage = valueToMaxDamage[value];

        let {
            curveTotal,
            damageToCurveValue,
        } = calculateCurveValues(value, maxDamage, absMaxDamage);

        let damageToData = mapValues(damageToCurveValue, (curveValue, damage) => {
            return {
                value,
                curveValue,
                damage: parseInt(damage, 10),
                percent: (curveValue / curveTotal) * 100,
            };
        });
        distribution[value] = calculateChanceValues(damageToData);
    });

    return distribution;
}

function calculateCurveValues(value, valueMaxDamage, absMaxDamage) {
    let damageToCurveValue = {};
    let curveTotal = 0;

    for (let i = 0; i <= valueMaxDamage; i++) {
        let dmg = i;
        // invert percent to be higher for lower values
        let dmgPercentOfMax = (absMaxDamage - dmg) / absMaxDamage;
        let curveValue = applyCurve(dmgPercentOfMax);
        damageToCurveValue[dmg] = curveValue;
        curveTotal += curveValue;
    }

    return {
        curveTotal,
        damageToCurveValue,
    };
}

function calculateChanceValues(damageToData) {
    let chance = 100;
    let prevPercent = 0;
    let total = 0;

    each(damageToData, (item) => {
        let percent = item.percent;

        chance -= prevPercent;
        item.chance = chance;

        prevPercent = percent;
        total += percent;
    });

    if (total < 99.99999999999999) {
        console.log('combat odds error, does not total 100 ' + total);
    }
    return damageToData;
}

function getSortedIntKeys(valueToMaxDamage) {
    return Object.keys(valueToMaxDamage)
        .map((key) => parseInt(key, 10))
        .sort((a, b) => a - b);
}

function applyCurve(x) {
    // ease in out quad
    return x < 0.5 ? 2 * x * x : 1 - Math.pow(-2 * x + 2, 2) / 2;
}
