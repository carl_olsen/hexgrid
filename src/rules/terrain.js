
import minBy from 'lodash/minBy';
import intersection from 'lodash/intersection';

import {cost, trailCost, traversable, transparent, modifiers} from '@/../data/terrain';

export function terrainDefense(unit, tile) {
    tile = tile || unit.tile;

    let modData = modifiers.defense[unit.type] || modifiers.defense.default;

    return modData[tile.type] || 0;
}

export function terrainTraversable(unit, tile) {
    let traversableData = traversable[unit.type] || traversable.default;
    let types          = tileTypes(tile);

    // inclusive (if any type is traversable then true)
    return !!types.find((type) => traversableData[type]);
}

export function terrainCost(unit, tile, prevTile) {
    let costData = cost[unit.type] || cost.default;
    let types    = tileTypes(tile);

    let matchingTrailCost = getMatchingTrailCost(unit, tile, prevTile);
    if (matchingTrailCost !== undefined) {
        return matchingTrailCost;
    }

    let total = 0;
    types.forEach((type) => {
        total += costData[type] || 0;
    });

    return total;
}

export function getMatchingTrailCost(unit, tile, prevTile) {
    if (!prevTile) {
        return;
    }
    let costData       = trailCost[unit.type] || trailCost.default;
    let types          = tileTypes(tile);
    let prevTypes      = tileTypes(prevTile);
    let trailTypes     = types.filter((type) => costData[type]);
    let prevTrailTypes = prevTypes.filter((type) => costData[type]);

    let matches = intersection(trailTypes, prevTrailTypes);

    if (!matches.length) {
        return;
    }

    let minTrailType = minBy(matches, (type) => costData[type]);

    return costData[minTrailType];
}

export function terrainTransparent(unit, tile) {
    let transparentData = transparent[unit.type] || transparent.default;
    let types           = tileTypes(tile);

    // exclusive (if any type is traversable then true)
    return !!types.find((type) => transparentData[type]);
}

export function tileTypes(tile) {
    return [].concat([tile.type], tile.subTypes || []);
}
