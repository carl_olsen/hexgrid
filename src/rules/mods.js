import round from 'lodash/round';
import {terrainDefense} from '@/rules/terrain';

export function CombatStat(attacking, key, unit, target) {

    let base = unit.stats[key];
    let stat = Stat(key, base);

    let targetTypeMod = TargetTypeMod(unit, key, target.type);
    if (targetTypeMod) {
        stat.addMod(targetTypeMod);
    }

    let hpPercent = unit.hp / unit.hpMax;
    let finalValueRaw = stat.value * hpPercent;
    let finalValue = Math.round(finalValueRaw);

    return Object.assign(stat, {
        hpPercent,
        finalValueRaw,
        finalValue,
    });
}

export function Stat(key, base = 0) {

    return {
        key,
        base,
        value: base,
        finalValue: 0,
        finalValueRaw: 0,
        mods: [],
        addMod({name, value}) {
            this.mods.push({name, value});
            this.value += value;
        },
    };
}


export function Modifier({name, key, description, source, value, type = 'int'}) {

    let displayValue = value;
    if (type === 'int' && value > 0) {
        displayValue = '+' + value;
    }

    if (type === 'percent') {
        displayValue = Math.round(value * 100) + '%';
    }
    return {
        name,
        key,
        description,
        type,
        source,
        value,
        displayValue,
    };
}

export function HpMod(unit, value) {

    let hpPercent = (unit.hp / unit.hpMax);
    if (hpPercent < 1) {
        hpPercent = round(hpPercent, 2);
        let percentLoss = round(1 - hpPercent, 2);
        let percentStr = round(percentLoss * 100) + '%';
        let percentFloat = round(percentLoss * value, 2);
        let percentInt = round(percentLoss * value);

        return Modifier({
            name: 'losses ' + percentStr + '<br> ( of ' + value + ' = ' + percentFloat + ' )',
            value: -percentInt,
        });
    }
}

export function TargetTypeMod(unit, key, targetType) {
    let base = unit.stats[key];
    let targetTypeStat = unit.stats[key + '_vs'][targetType];
    if (typeof targetTypeStat !== 'number') {
        return;
    }

    let value = targetTypeStat - base;

    if (value === 0) {
        return;
    }

    return Modifier({
        name: 'vs ' + targetType,
        key,
        value,
    });
}

export function TerrainDefenseMod(unit, tile) {
    tile = tile || unit.tile;
    let terrainType = tile.type;
    let value = terrainDefense(unit, tile);

    if (!value) {
        return;
    }

    return Modifier({
        name: 'in ' + terrainType,
        value,
    });
}
