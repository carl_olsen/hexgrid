import {terrainTraversable} from '@/rules/terrain';

export function isFriendly(unit, otherUnit) {
    return unit.ownerId === otherUnit.ownerId;
}

export function isEnemy(unit, otherUnit) {
    return unit.ownerId !== otherUnit.ownerId;
}

export function checkTileMovement(unitsByTileId, unit, tile) {
    let unitInTile = unitsByTileId[tile.id];
    let containsEnemy = unitInTile && isEnemy(unit, unitInTile);
    if (containsEnemy) {
        return {
            canMoveThrough: false,
            canOccupy: false,
        };
    }

    let canTraverseTerrain = terrainTraversable(unit, tile);
    if (!canTraverseTerrain) {
        return {
            canMoveThrough: false,
            canOccupy: false,
        };
    }
    let containsFriendly = unitInTile && !containsEnemy;
    if (containsFriendly) {
        return {
            canMoveThrough: true,
            canOccupy: false,
        };
    }

    return {
        canMoveThrough: true,
        canOccupy: true,
    };
}

export function isValidTarget(grid, source, target, sourceTile = null, targetTile = null) {
    sourceTile = sourceTile || source.tile;
    targetTile = targetTile || target.tile;

    if (source === target) {
        return false;
    }

    if (target.ownerId === source.ownerId) {
        return false;
    }

    let dist = grid.distanceTo(sourceTile, targetTile);
    let min = source.attackMinRange;
    let max = source.attackRange;

    let inMinRange = true;
    if (min) {
        inMinRange = min <= dist;
    }
    let inMaxRange = dist <= max;

    return inMinRange && inMaxRange;
}
