import styleConf from '@/../sass/style-config.scss';

let {
    hexSize,
    unitSize,
    unitHpBarHeight,
    unitHpBarMargin,
    unitNatoWidth,
    unitNatoHeight,
    mapMargin,
} = styleConf;

styleConf.hexSize = parseFloat(hexSize);
styleConf.unitSize = parseFloat(unitSize);
styleConf.unitHpBarHeight = parseFloat(unitHpBarHeight);
styleConf.unitHpBarMargin = parseFloat(unitHpBarMargin);
styleConf.unitNatoWidth = parseFloat(unitNatoWidth);
styleConf.unitNatoHeight = parseFloat(unitNatoHeight);
styleConf.mapMargin = parseFloat(mapMargin);

export const styleConfig = styleConf
export const DEBUG_PATHS = false;
