import Vue from 'vue';
import CombatData from '@/vue/components/CombatData';

let vueMainUnitData = new Vue({
    el: '#combat-data',
    render: h => h(CombatData),
});
