import Vue from 'vue';
import UnitData from '@/vue/components/UnitData';

let vueMainUnitData = new Vue({
    el: '#unit-data',
    render: h => h(UnitData),
});
