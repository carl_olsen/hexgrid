//

// module.exports = function(keys) {
//     return new MultiObjectManager(keys);
// };

// function MultiObjectManager(keys = []) {
//     this.objects      = [];
//     this.objectsByKey = {};

//     keys.forEach(function(key) {
//         this.objectsByKey[key] = [];
//     }, this);
// }

// MultiObjectManager.proto = {

//     /**
//     * Retrieves all objects at given map tile coord.
//     * @method get
//     * @param {Object} tile - The tile
//     * @param {Function} [filter] - A function to filter the array of objects returned `function(object){  return true }`.
//     * @return {Array}
//     */
//     get: function(tile, filter) {
//         let objects = this.objectsByKey[tile.key];

//         if (filter && objects) {
//             return objects.filter(filter);

//         }
//         return objects;

//     },

//     getFirst: function(tile, filter) {
//         let arr = this.objectsByKey[tile.key];

//         if (arr) {
//             if (filter) {
//                 let len = arr.length;
//                 for (let i = 0; i < len; i++) {
//                     let item = arr[i];
//                     if (filter(item)) {
//                         return item;
//                     }
//                 }
//             } else {
//                 return arr[0];
//             }
//         }
//     },

//     getLast: function(tile, filter) {
//         let arr = this.objectsByKey[tile.key];
//         if (arr) {
//             if (filter) {
//                 for (let i = arr.length - 1; i >= 0; i--) {
//                     let item = arr[i];
//                     if (filter(item)) {
//                         return item;
//                     }
//                 }
//             } else {
//                 return arr[arr.length - 1];
//             }
//         }
//     },

//     add: function(tile, obj) {
//         obj.tile = tile;
//         let arr = this.objectsByKey[tile.key];
//         this.objects.push(obj);
//         arr.push(obj);
//         return obj;
//     },

//     remove: function(obj) {
//         let index;
//         if (obj.tile) {

//             let arr = this.objectsByKey[obj.tile.key];
//             index = arr.indexOf(obj);
//             arr.splice(index, 1);
//         }

//         index = this.objects.indexOf(obj);
//         this.objects.splice(index, 1);

//     },

//     removeAt: function(tile, filter) {
//         let arr = this.get(tile, filter)
//         for (let i = arr.length - 1; i >= 0; i--) {
//             this.remove(arr[i]);
//         }
//     },

//     move: function(tile, object) {
//         this.remove(object);
//         this.add(tile, object);
//     },

// }
