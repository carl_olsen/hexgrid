import Cube from '@/hexgrid/cube';

export default function calculatePath({startTile, endTile, worldSize, getNeighbors, maxMove, isPathableTile, node, getTileCost}) {

    getTileCost    = getTileCost || (() => 1);
    isPathableTile = isPathableTile || (() => true);
    node           = node || makeNode;

    let startNode = node(null, startTile);
    let endNode   = node(null, endTile);

    // create an array that will contain all world cells
    let AStar = [];
    // list of currently open Nodes
    let Open = [startNode];
    // list of closed Nodes
    let Closed = [];
    // list of the final output array
    let result = [];

    // reference to a Node (that we are considering now)
    let myNode;
    // reference to a Node (that starts a path in question)
    let length, max, min, i;
    // iterate through the open list until none are left
    while (length = Open.length) {
        max = worldSize;
        min = -1;
        for (i = 0; i < length; i++) {
            if (Open[i].f < max) {
                max = Open[i].f;
                min = i;
            }
        }
        // grab the next node and remove it from Open array
        myNode = Open.splice(min, 1)[0];
        // is it the destination node?
        if (myNode.value === endNode.value) {
            let path = Closed[Closed.push(myNode) - 1];
            do {
                result.push(path);
            } while (path = path.parent);
            // clear the working arrays
            AStar = Closed = Open = [];
            // we want to return start to finish
            result.reverse();
        } else {

            let neighbors = getNeighbors(myNode.tile);

            neighbors
                .filter(function(nTile) {
                    return isPathableTile(nTile);
                })
                .forEach(function(nTile) {
                    let path = node(myNode, nTile);
                    if (!AStar[path.value]) {
                        let distToEnd     = getDistance(nTile, endNode.tile);
                        let pathCostSoFar = myNode.g;
                        let tileMoveCost  = path.tileMoveCost;

                        // cost of this particular route so far
                        path.g = pathCostSoFar + tileMoveCost;
                        // estimated cost of entire guessed route to the destination
                        path.f = path.g + distToEnd;

                        if (!maxMove || path.g <= maxMove) {
                            // remember this new path for testing above
                            Open.push(path);
                        }
                        // mark this node in the world graph as visited
                        AStar[path.value] = true;
                    }
                });

            // remember this route as having no more untested options
            Closed.push(myNode);
        }
    } // keep iterating until the Open list is empty
    return result;

    // Node function, returns a new object with Node properties
    // Used in the calculatePath function to store route costs, etc.
    function makeNode(parent, tile) {

        let tileMoveCost;
        if (tile === startTile) {
            tileMoveCost = 0;
        } else {
            let prevTile;
            if(parent){
                prevTile = parent.tile;
            }
            tileMoveCost = getTileCost(tile, prevTile);
        }
        return {
            // pointer to another Node object
            parent: parent,
            tile:   tile,
            value:  tile.key,

            // the heuristic estimated cost
            // of an entire path using this node
            f: 0,
            // the distance cost to get
            // from the starting point to this node
            g: 0,

            // arbitrary cost
            tileMoveCost
        };

    }


}


function getDistance(tileA, tileB) {
    return Cube.distance(tileA, tileB);
}
